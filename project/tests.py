import os

from django.conf import settings

from django.test import TestCase
from django.contrib.auth.models import User

from django.core.files.uploadedfile import SimpleUploadedFile

from .models import Application
from .models import Project
from .models import Team
from .models import Member

from .forms import ApplicationDraftForm
from .forms import ApplicationPublishForm
from .forms import ProjectDraftForm
from .forms import ProjectPublishForm
from .forms import TeamDraftForm
from .forms import TeamPublishedForm
from .forms import TeamMemberForm

from project.subtests.test_view_application import (
    ApplicationCreateViewTest,
    ApplicationListViewTest,
    ApplicationViewGetMethodTest,
    ApplicationViewPostMethodTest,
    ApplicationFinalizeViewTest
)

from project.subtests.test_view_project import (
    ProjectTemplateViewTest,
    ProjectViewGetMethonTest,
    ProjectViewPostMethonTest,
    ProjectFinalizeViewTest,
    ProjectShownViewTest
)

from project.subtests.test_view_team import (
    TeamMemberViewTest,
    TeamPublishedViewTest
)


# Create your tests here.
class ApplicationValidDraftFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True

    def test_valid_application_form(self):
        # whole form
        post_data = {
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertTrue(form.is_valid(), msg='form must be valid but logic has denied it.')

    def test_valid_qatari_partnership(self):
        post_data = {
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertTrue(form.is_valid(), msg='`qatari_partnership` must be valid but logic has denied it.')

    def test_valid_website(self):
        post_data = {
            'website': 'deneb.com',
        }
        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertTrue(form.is_valid(), msg='`website` must be valid but logic has denied it.')

    def test_valid_industry(self):
        post_data = {
            'industry': 'Art'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertTrue(form.is_valid(), msg='`industry` must be valid but logic has denied it.')

    def test_valid_deneb_result(self):
        post_data = {
            'deneb_result': 'Techshow'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertTrue(form.is_valid(), msg='`deneb_result` must be valid but logic has denied it.')

    def test_valid_sales_rate_type(self):
        post_data = {
            'sales_rate_type': 'm'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertTrue(form.is_valid(), msg='`sales_rate_type` must be valid but logic has denied it.')

class ApplicationInvalidDraftFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True

    def test_invalid_qatari_partnership(self):
        post_data = {
            'qatari_partnership': True,
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`qatari_partnership` must be invalid but logic has accepted.')

    def test_invalid_website(self):
        post_data = {
            'website': 'qwer',
        }
        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`website` must be invalid but logic has accepted.')

    def test_invalid_industry(self):
        post_data = {
            'industry': 'Something Else'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`website` must be invalid but logic has accepted.')

    def test_invalid_deneb_result(self):
        post_data = {
            'deneb_result': 'Something Else'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`deneb_result` must be invalid but logic has accepted.')

    def test_invalid_sales_rate_type(self):
        post_data = {
            'sales_rate_type': 'Something Else'
        }

        form = ApplicationDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`sales_rate_type` must be invalid but logic has accepted.')

class ApplicationValidPublishedFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.__create_published_profile()
        self.__create_draft_application()
        self.application = Application.objects.last()


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()
    

    
    
    def __create_draft_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        Application.objects.create(**data)

    def test_valid_profile_form(self):
        post_data = {
            'is_published': True
        }
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertTrue(form.is_valid(), msg='`is_published` must be valid but logic has denied it.')

class ApplicationInvalidPublishedFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.__create_draft_application()
        self.application = Application.objects.last()

    def __create_draft_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': False,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        Application.objects.create(**data)

    def test_invalid_profile_form(self):
        post_data = {
            'is_published': True
        }
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`qatari_partnership` must be invalid but logic has accepted.')

    def test_invalid_industry_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.industry = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`industry` must be invalid but logic has accepted.')


    def test_invalid_deneb_result_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.deneb_result = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`deneb_result` must be invalid but logic has accepted.')

    def test_invalid_abstract_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.abstract = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`abstract` must be invalid but logic has accepted.')

    def test_invalid_abstract_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.abstract = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`abstract` must be invalid but logic has accepted.')

    def test_invalid_sales_rate_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.sales_rate = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`sales_rate` must be invalid but logic has accepted.')

    def test_invalid_sales_rate_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.sales_rate = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`sales_rate` must be invalid but logic has accepted.')

    def test_invalid_sales_rate_type_null(self):
        post_data = {
            'is_published': True
        }
        self.application.is_b2b = True
        self.application.sales_rate_type = None

        self.application.save()
        form = ApplicationPublishForm(post_data, instance=self.application)
        self.assertFalse(form.is_valid(), msg='`sales_rate_type` must be invalid but logic has accepted.')

class ProjectValidDraftFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.app = self.__create_published_application()

    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app
    
    def test_valid_project_form(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                post_data = {
                    'application': self.app,
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                }

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()

                file_data = {
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                }

                form = ProjectDraftForm(post_data, file_data, user = self.user)
                print(form.errors)
                self.assertTrue(form.is_valid(), msg='form must be valid but logic has denied it.')

class ProjectValidPublishFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.user.profile.is_published = True
        self.user.profile.save()
        self.app = self.__create_published_application()
        self.project = self.__create_draft_project()

    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app

    def __create_draft_project(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                data = {
                    'application': self.app,
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'problem': 'lorem ipsum',
                    'solution': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                }

                project = Project.objects.create(**data)
                return project 

    def test_valid_profile_form(self):
        post_data = {
            'is_published': True
        }
        form = ProjectPublishForm(post_data, instance=self.project)
        print(form.errors)
        self.assertTrue(form.is_valid(), msg='`is_published` must be valid but logic has denied it.')

class TeamValidDraftFormTest(TestCase):
    
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.app = self.__create_published_application()
        self.project = self.__create_published_project()

    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app

    def __create_published_project(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                data = {
                    'application': self.app,
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'is_published': True
                }

                project = Project.objects.create(**data)
                return project 

    def test_valid_team_form(self):
        
        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': self.project,
            'is_published': False
        }

        form = TeamDraftForm(data, instance = self.user)

        self.assertTrue(
            form.is_valid(),
            msg='Form must be valid, but logic has denied it.'
        )

class TeamInvalidDraftFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.app = self.__create_published_application()
        self.project = self.__create_published_project()

    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app

    def __create_published_project(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                data = {
                    'application': self.app,
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'is_published': True
                }

                project = Project.objects.create(**data)
                return project 

    def test_invalid_name(self):
        data = {
            'name': '',
            'user': self.user,
            'project': self.project,
            'is_published': False
        }

        form = TeamDraftForm(data, instance = self.user)

        self.assertFalse(
            form.is_valid(),
            msg='The team should not be nameless.'
        )
    
class TeamValidPublishedFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.app = self.__create_published_application()
        self.project = self.__create_published_project()

        self.team = self.__create_team()
    
    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app
    
    def __create_published_project(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                data = {
                    'application': self.app,
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'is_published': True
                }

                project = Project.objects.create(**data)
                return project 

    def __create_team(self):
        
        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': self.project,
            'is_published': False
        }

        team = Team.objects.create(**data)

        return team

    def __create_team_member(self):
            
        data = {
            'first_name': 'sepehr',
            'last_name': 'ak',
            'position': 'CEO',
            'phone_number': '+989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }

        team_member = Member.objects.create(**data)

    def test_valid_published_team_form(self):

        self.__create_team_member()

        data = {
            'is_published': True
        }

        form = TeamPublishedForm(data, instance=self.team)

        self.assertTrue(form.is_valid(), msg='`is_published` must be valid but logic has denied it.')    

class MemberValidDraftFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
        self.app = self.__create_published_application()
        self.project = self.__create_published_project()

        self.team = self.__create_team()

     
    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app
    
    def __create_published_project(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                data = {
                    'application': self.app,
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'is_published': True
                }

                project = Project.objects.create(**data)
                return project 

    def __create_team(self):
        
        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': self.project,
            'is_published': False
        }

        team = Team.objects.create(**data)

        return team

    def test_valid_member_form(self):
        data = {
            'first_name': 'sepehr',
            'last_name': 'ak',
            'position': 'CEO',
            'phone_number': '+989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }
        

        form = TeamMemberForm(data)

        print(form.errors)

        self.assertTrue(
            form.is_valid(),
            msg='Form must be valid, but logic has denied it.'
        )
