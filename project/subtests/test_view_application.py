import os

from decouple import config

from django.test import (
    TestCase,
    Client
)

from django.conf import settings

from django.core.files.uploadedfile import SimpleUploadedFile

from django.contrib.auth.models import User

from django.forms.models import model_to_dict

from django.urls import reverse

from project.models import (
    Application,
    Project,
    Team,
    Member
)

from project.forms import ApplicationDraftForm


class ApplicationCreateViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True

        # self.client = Client(enforce_csrf_checks = True)

    
    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()
    
    
    def test_without_login(self):

        data = {
            'qatari_partnership': True,
            'kafil_fname': 'hamed',
            'kafil_lname': 'babagheybi',
            'kafil_email': 'hmdbbgh1011@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        response = self.client.post(
            '/dashboard/account/application/{}/create/'.format(self.user.pk),
            data=data
        )

        self.assertEqual(response.status_code, 302)
    
    
    def test_is_not_published_profile_request(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        data = {
            'qatari_partnership': True,
            'kafil_fname': 'hamed',
            'kafil_lname': 'babagheybi',
            'kafil_email': 'hmdbbgh1011@gmail.com',
            'kafil_qatari_id': '950145100',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        response = self.client.post(
            '/dashboard/account/application/{}/create/'.format(self.user.pk),
            data=data,
            follow=True
        )
        
        self.assertRedirects (
            response,
            reverse('dashboard:application-list'),
            status_code = 302,
            target_status_code = 200,
            msg_prefix = "The user has not published his profile. He should not be able to create a new application.",
            fetch_redirect_response = True
        )

        self.assertIn(
            b'You must first finalize your profile',
            response.content
        )

    
    # def test_is_published_profile_request(self):

    #     login_flag = self.client.login(username='hamed_test', password='h@med_1234')

    #     self.assertTrue(
    #         login_flag,
    #         msg = 'You could not log in.'
    #     )
        
    #     self.__create_published_profile()

    #     data = {
    #         'qatari_partnership': True,
    #         'kafil_fname': 'hamed',
    #         'kafil_lname': 'babagheybi',
    #         'kafil_email': 'hmdbbgh1011@gmail.com',
    #         'kafil_qatari_id': '950145100',
    #         'website': 'deneb.com',
    #         'industry': 'Art',
    #         'deneb_result': 'Techshow',
    #         'abstract': 'lorem ipsum',
    #         'is_b2b': True,
    #         'has_product': True,
    #         'sales_rate': '10000000',
    #         'sales_rate_type': 'm',
    #         'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr',
    #         'g-recaptcha-response': config('RECAPTCHA_SITE_KEY')
    #     }

    #     response = self.client.post(
    #         '/dashboard/account/application/{}/create/'.format(self.user.pk),
    #         data=data,
    #         follow=True
    #     )

    #     self.assertEqual(
    #         Application.objects.filter(user = self.user).count(),
    #         2,
    #         'The user must have 2 applications'
    #     )

    #     new_app = Application.objects.all().order_by('-created')[0]

    #     self.assertRedirects (
    #         response,
    #         reverse('dashboard:application-edit', kwargs={'sku':new_app.sku}),
    #         status_code = 302,
    #         target_status_code = 200,
    #         msg_prefix = "The user should be able to create a new application.",
    #         fetch_redirect_response = True
    #     )


class ApplicationListViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True

    
    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()
    
    
    def test_without_login(self):

        response = self.client.get(
            '/dashboard/account/applications/'
        )

        self.assertRedirects (
            response,
            '/accounts/login?next=/dashboard/account/applications/',
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )

    
    def test_after_login(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            '/dashboard/account/applications/'
        )

        self.assertEqual(response.status_code, 200)

        self.assertContains(
            response,
            'applications',
            status_code = 200,
            msg_prefix = 'applications key must be in the response contaxt'
        )

        self.assertEqual(
            len(response.context.get('applications')),
            1,
            msg = 'The user must have one application'
        )

        self.__create_published_profile()

        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'hamed',
            'kafil_lname': 'babagheybi',
            'kafil_email': 'hmdbbgh1011@gmail.com',
            'kafil_qatari_id': '950145100',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr',
        }

        Application.objects.create(**data)

        response = self.client.get(
            '/dashboard/account/applications/'
        )

        self.assertEqual(
            len(response.context.get('applications')),
            2,
            msg = 'The user must have 2 applications'
        )


class ApplicationViewGetMethodTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

        self.user.profile.email_confirmed = True

    
    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()
    
    
    def __create_published_application(self):

        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr',
            'is_published': True
        }

        app = Application.objects.create(**data)
        return app
    
    
    def __create_draft_project(self, app):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = False
                
                app.project.save()

                self.project = app.project

  
    def __create_published_project(self):
        
        self.project.is_published = True
        self.project.save()

    
    def __create_team(self):
        
        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': self.project,
            'is_published': False
        }

        self.team = Team.objects.create(**data)
    
    
    def __create_team_member(self):

        data = {
            'first_name': 'sepehr',
            'last_name': 'akbarzade',
            'position': 'CEO',
            'phone_number': '+0989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }

        self.team_member = Member.objects.create(**data)

    
    def __create_published_team(self):

        self.team.is_published = True

        self.team.save()
    
    
    def __create_is_shown_project(self):

        self.project.is_shown = True
        
        self.project.save()

    
    def test_without_login(self):

        self.__create_published_profile()

        response = self.client.get(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            )
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-edit',
                    kwargs = {'sku': self.user.applications.all()[0].sku}
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )


    def test_after_login_draft_application(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_published_profile()

        app = self.user.applications.all()[0]

        response = self.client.get(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': app.sku}
            ),
            follow=True
        )

        expected_fields = (
            'form',
            'view',
            'application',
            'current_step',
            'project',
            'project_form',
            'team',
            'profile_form',
        )

        for field in expected_fields:

            self.assertIn(
                field,
                response.context,
                msg = '{} key must be in the response contaxt'.format(field)
            )

        self.assertEqual(
            response.context['view']['page_name'],
            'Application',
            msg = 'value of view key of response context is incorrect'
        )
        
        self.assertEqual(
            response.context['current_step'],
            1,
            msg = 'current_step value must be 1'
        )

        self.assertEqual(
            response.context['profile_form'].user,
            self.user,
            msg = 'profile form is incorrect'
        )

        self.assertFalse(
            response.context['project'],
            msg = 'There should be no project'  
        )

        self.assertFalse(
            response.context['project_form'],
            msg = 'There should be no project_form'  
        )

        self.assertFalse(
            response.context['team'],
            msg = 'There should be no team'  
        )

        self.assertEqual(
            response.context['application'],
            app,
            msg = 'The user application and application key value of response context must be same.'
        )

        # print(dir(response.context['form']))
        # print(response.context['profile_form'].user)
        # print(response.context['form'].__class__.__name__)

        # self.assertEqual(
        #     response.context['form'],
        #     ApplicationDraftForm(
        #         initial = model_to_dict(app),
        #         instance = self.user,
        #     )
        # )

    
    def test_published_application(self):
        
        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_published_profile()

        app = self.__create_published_application()

        response = self.client.get(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': app.sku}
            ),
            follow=True
        )

        expected_fields = (
            'form',
            'view',
            'application',
            'current_step',
            'project',
            'project_form',
            'team',
            'profile_form',
        )

        for field in expected_fields:

            self.assertIn(
                field,
                response.context,
                msg = '{} key must be in the response contaxt'.format(field)
            )

        self.assertEqual(
            response.context['view']['page_name'],
            'Application',
            msg = 'value of view key of response context is incorrect'
        )
        
        self.assertEqual(
            response.context['current_step'],
            2,
            msg = 'current_step value must be 2'
        )

        self.assertEqual(
            response.context['profile_form'].user,
            self.user,
            msg = 'profile form is incorrect'
        )

        self.assertTrue(
            response.context['project'],
            msg = 'There should be a project'  
        )

        self.assertTrue(
            response.context['project_form'],
            msg = 'There should be a project_form'  
        )
        
        self.assertFalse(
            response.context['team'],
            msg = 'There should be no team'  
        )

        self.assertEqual(
            response.context['application'],
            app,
            msg = 'The user application and application key value of response context must be same.'
        )


    def test_published_project(self):

        self.__create_published_profile()

        app = self.__create_published_application()
       
        self.__create_draft_project(app)
        
        self.__create_published_project()

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': app.sku}
            ),
            follow=True
        )

        expected_fields = (
            'form',
            'view',
            'application',
            'current_step',
            'project',
            'project_form',
            'team',
            'profile_form',
        )

        for field in expected_fields:

            self.assertIn(
                field,
                response.context,
                msg = '{} key must be in the response contaxt'.format(field)
            )

        self.assertEqual(
            response.context['view']['page_name'],
            'Application',
            msg = 'value of view key of response context is incorrect'
        )

        self.assertEqual(
            response.context['current_step'],
            3,
            msg = 'current_step value must be 3'
        )

        self.assertEqual(
            response.context['profile_form'].user,
            self.user,
            msg = 'profile form is incorrect'
        )

        self.assertTrue(
            response.context['project'],
            msg = 'There should be a project'  
        )

        self.assertTrue(
            response.context['project_form'],
            msg = 'There should be a project_form'  
        )
        
        self.assertFalse(
            response.context['team'],
            msg = 'There should be no team'  
        )

        self.assertEqual(
            response.context['application'],
            app,
            msg = 'The user application and application key value of response context must be same.'
        )

    
    def test_team_published_project(self):

        self.__create_published_profile()

        app = self.__create_published_application()
       
        self.__create_draft_project(app)
        
        self.__create_published_project()

        self.__create_team()
        
        self.__create_team_member()

        self.__create_published_team()

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': app.sku}
            ),
            follow=True
        )

        expected_fields = (
            'form',
            'view',
            'application',
            'current_step',
            'project',
            'project_form',
            'team',
            'profile_form',
        )

        for field in expected_fields:

            self.assertIn(
                field,
                response.context,
                msg = '{} key must be in the response contaxt'.format(field)
            )

        self.assertEqual(
            response.context['view']['page_name'],
            'Application',
            msg = 'value of view key of response context is incorrect'
        )

        self.assertEqual(
            response.context['current_step'],
            4,
            msg = 'current_step value must be 4'
        )

        self.assertEqual(
            response.context['profile_form'].user,
            self.user,
            msg = 'profile form is incorrect'
        )

        self.assertTrue(
            response.context['project'],
            msg = 'There should be a project'  
        )

        self.assertTrue(
            response.context['project_form'],
            msg = 'There should be a project_form'  
        )
        
        self.assertTrue(
            response.context['team'],
            msg = 'There should be a team'  
        )

        self.assertEqual(
            response.context['application'],
            app,
            msg = 'The user application and application key value of response context must be same.'
        )


    def test_is_shown_project(self):

        self.__create_published_profile()

        app = self.__create_published_application()
       
        self.__create_draft_project(app)
        
        self.__create_published_project()

        self.__create_team()
        
        self.__create_team_member()

        self.__create_published_team()

        self.__create_is_shown_project()

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': app.sku}
            ),
            follow=True
        )

        expected_fields = (
            'form',
            'view',
            'application',
            'current_step',
            'project',
            'project_form',
            'team',
            'profile_form',
        )

        for field in expected_fields:

            self.assertIn(
                field,
                response.context,
                msg = '{} key must be in the response contaxt'.format(field)
            )

        self.assertEqual(
            response.context['view']['page_name'],
            'Application',
            msg = 'value of view key of response context is incorrect'
        )

        self.assertEqual(
            response.context['current_step'],
            5,
            msg = 'current_step value must be 5'
        )

        self.assertEqual(
            response.context['profile_form'].user,
            self.user,
            msg = 'profile form is incorrect'
        )

        self.assertTrue(
            response.context['project'],
            msg = 'There should be a project'  
        )

        self.assertTrue(
            response.context['project_form'],
            msg = 'There should be a project_form'  
        )
        
        self.assertTrue(
            response.context['team'],
            msg = 'There should be a team'  
        )

        self.assertEqual(
            response.context['application'],
            app,
            msg = 'The user application and application key value of response context must be same.'
        )


class ApplicationViewPostMethodTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

        self.user.profile.email_confirmed = True
    

    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()
    
    
    def test_without_login(self):

        self.__create_published_profile()

        data = {
            'qatari_partnership': True,
            'kafil_fname': 'hamed',
            'kafil_lname': 'babagheybi',
            'kafil_email': 'hmdbbgh1011@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'hamed, babagheybi, kashmar'
        }

        response = self.client.post(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-edit',
                    kwargs = {'sku': self.user.applications.all()[0].sku}
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )


    def test_after_login_qatari_partnership_invalid(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_published_profile()

        data = {
            'qatari_partnership': False,
            'kafil_fname': 'hamed',
            'kafil_lname': 'babagheybi',
            'kafil_email': 'hmdbbgh1011@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '2000',
            'sales_rate_type': 'm',
            'keywords': 'hamed, babagheybi, kashmar'
        }

        response = self.client.post(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            data = data,
            follow=True
        )
        
        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "Qatari Partnership is not valid",
            fetch_redirect_response = True
        )

        self.assertIn(
            b'You must select True for Qatari Partnership if you have a kafil.',
            response.content
        )


    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_published_profile()

        data = {
            'qatari_partnership': True,
            'kafil_fname': 'hamed',
            'kafil_lname': 'babagheybi',
            'kafil_email': 'hmdbbgh1011@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'has_product': True,
            'sales_rate': '2000',
            'sales_rate_type': 'm',
            'keywords': 'hamed, babagheybi, kashmar'
        }

        response = self.client.post(
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            data = data,
            follow=True
        )
        
        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "All thing is good!",
            fetch_redirect_response = True
        )

        self.assertIn(
            b'Your application has been changed.',
            response.content
        )


class ApplicationFinalizeViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

        self.user.profile.email_confirmed = True
    
    
    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()
    

    def __update_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'

        app.save()
    
    
    def test_without_login(self):

        data = {
            'is_published': True,
        }

        response = self.client.post(
            reverse(
                'dashboard:application-published',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-published',
                    kwargs = {'sku': self.user.applications.all()[0].sku}
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )

    
    def test_after_login_is_not_published_profile(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        data = {
            'is_published': True,
        }

        response = self.client.post(
            reverse(
                'dashboard:application-published',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            data = data,
            follow = True
        )

        self.assertIn(
            b'You must finalize your profile.',
            response.content
        )

        
    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        data = {
            'is_published': True,
        }

        self.__create_published_profile()

        self.__update_first_application()

        response = self.client.post(
            reverse(
                'dashboard:application-published',
                kwargs = {'sku': self.user.applications.all()[0].sku}
            ),
            data = data,
            follow = True
        )
        
        self.assertIn(
            b'Your application is finalized.',
            response.content
        )