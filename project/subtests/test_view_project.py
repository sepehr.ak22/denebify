import os

from django.conf import settings

from django.test import TestCase

from django.urls import reverse

from django.contrib.auth.models import User

from django.core.files.uploadedfile import SimpleUploadedFile

from project.models import (
    Project,
    Team,
    Member
)


class ProjectTemplateViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app
    

    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        response = self.client.get(
            reverse('dashboard:project-view', kwargs = {
                                                'sku': app.sku,
                                                'sku_project': app.project.sku
                                            }
            )
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse('dashboard:project-view', kwargs = {
                                                    'sku': app.sku,
                                                    'sku_project': app.project.sku
                                                }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )


    def test_after_login(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_published_profile()

        app = self.__publishe_first_application()

        response = self.client.get(
            reverse('dashboard:project-view', kwargs = {
                                                'sku': app.sku,
                                                'sku_project': app.project.sku
                                            }
            ),
            follow = True
        )

        self.assertContains(
            response,
            'view',
            status_code = 200,
            msg_prefix = 'view key must be in the response contaxt'
        )

        self.assertEqual(
            'Application',
            response.context.get('view')['page_name'],
            msg = 'page_name is incorrect.'
        )

        self.assertTrue(
            response.context.get('view')['remove_footer'],
            msg = 'remove_footer must be True.'
        )

        self.assertIn(
            'project',
            response.context.keys(),
            msg = 'project key must be in the response contaxt'
        )

        self.assertEqual(
            app.project,
            response.context.get('project'),
            msg = 'project is incorrect.'
        )


class ProjectViewGetMethonTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app
    

    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        response = self.client.get(
            reverse(
                'dashboard:application-project-edit',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            )
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-project-edit',
                    kwargs = {
                        'sku': app.sku,
                        'sku_project': app.project.sku
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )


    def test_after_login(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_published_profile()

        app = self.__publishe_first_application()

        response = self.client.get(
            reverse(
                'dashboard:application-project-edit',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            follow = True
        )

        self.assertContains(
            response,
            'form',
            status_code = 200,
            msg_prefix = 'view key must be in the response contaxt'
        )

        self.assertContains(
            response,
            'view',
            status_code = 200,
            msg_prefix = 'view key must be in the response contaxt'
        )

        self.assertEqual(
            'Project',
            response.context.get('view')['page_name'],
            msg = 'page_name is incorrect.'
        )

        self.assertIn(
            'project',
            response.context.keys(),
            msg = 'project key must be in the response contaxt'
        )

        self.assertEqual(
            app.project,
            response.context.get('project'),
            msg = 'project is incorrect.'
        )


class ProjectViewPostMethonTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app
    

    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()

                data = {
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                }

        response = self.client.post(
            reverse(
                'dashboard:application-project-edit',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-project-edit',
                    kwargs = {
                        'sku': app.sku,
                        'sku_project': app.project.sku
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )

    
    def test_after_login_valid_data_not_confirmed_email(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()

                data = {
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                }

        response = self.client.post(
            reverse(
                'dashboard:application-project-edit',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "The user email not confirmed.",
            fetch_redirect_response = True
        )


    def test_after_login_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.user.profile.save()

        app = self.__publishe_first_application()

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()

                data = {
                    'title': 'deneb',
                    'summary': 'lorem ipsum',
                    'product': 'lorem ipsum',
                    'customer': 'lorem ipsum',
                    'business': 'lorem ipsum',
                    'market': 'lorem ipsum',
                    'competition': 'lorem ipsum',
                    'vision': 'lorem ipsum',
                    'founders': 'lorem ipsum',
                    'cover': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'video': SimpleUploadedFile(uploaded_video_name, uploaded_video_file),
                    'summary_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'product_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'customer_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'business_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'market_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'competition_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'vision_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'founders_img': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                }

        response = self.client.post(
            reverse(
                'dashboard:application-project-edit',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertIn(
            b"Your project has been changed.",
            response.content
        )

        self.assertEqual(
            response.status_code,
            200
        )


class ProjectFinalizeViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True
        self.user.profile.is_published = True
        self.user.profile.save()


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app


    def __create_draft_project(self, app):
        
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.problem = 'lorem ipsum'
                app.project.solution = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = False
                
                app.project.save() 
    
    
    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        self.__create_draft_project(app)

        data = {
            'is_published': True
        }

        response = self.client.post(
            reverse(
                'dashboard:application-project-published',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-project-published',
                    kwargs = {
                        'sku': app.sku,
                        'sku_project': app.project.sku
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )


    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_draft_project(app)

        data = {
            'is_published': True
        }

        response = self.client.post(
            reverse(
                'dashboard:application-project-published',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )
        
        project = Project.objects.get(application = app)

        self.assertTrue(
            project.is_published,
            msg = 'project must be published'
        )

        self.assertIn(
            b'Your project is finalized.',
            response.content
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )


class ProjectShownViewTest(TestCase):
    
    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True
        self.user.profile.is_published = True
        self.user.profile.save()


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app


    def __create_published_project(self, app):
        
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.problem = 'lorem ipsum'
                app.project.solution = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = True
                
                app.project.save() 


    def __create_team(self, project):

        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': project,
            'is_published': False
        }

        self.team = Team.objects.create(**data)
    
    
    def __create_team_member(self):

        data = {
            'first_name': 'sepehr',
            'last_name': 'akbarzade',
            'position': 'CEO',
            'phone_number': '+0989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }

        self.team_member = Member.objects.create(**data)

    
    def __create_published_team(self):

        self.team.is_published = True

        self.team.save()  
    
    
    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        data = {
            'is_shown': True
        }

        response = self.client.post(
            reverse(
                'dashboard:application-project-shown',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:application-project-shown',
                    kwargs = {
                        'sku': app.sku,
                        'sku_project': app.project.sku
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )


    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        self.__create_team(app.project)

        self.__create_team_member()

        self.__create_published_team()

        data = {
            'is_shown': True
        }

        response = self.client.post(
            reverse(
                'dashboard:application-project-shown',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )

        project = Project.objects.get(application = app)

        self.assertTrue(
            project.is_shown,
            msg = 'project must be showned'
        )

        self.assertIn(
            b'Your project has been shown in site.',
            response.content
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )