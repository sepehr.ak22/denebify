import os

from django.conf import settings

from django.test import TestCase

from django.urls import reverse

from django.contrib.auth.models import User

from django.core.files.uploadedfile import SimpleUploadedFile

from project.models import (
    Project,
    Team,
    Member
)


class TeamMemberViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True
        self.user.profile.save()


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app


    def __create_published_project(self, app):
        
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.problem = 'lorem ipsum'
                app.project.solution = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = True
                
                app.project.save() 


    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        data = {
            'is_shown': True
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-member',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:project-team-member',
                    kwargs = {
                        'sku': app.sku,
                        'sku_project': app.project.sku
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )


    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        data = {
            'name': 'Sage',
            'form-TOTAL_FORMS': '2',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-first_name': 'sepehr',
            'form-0-last_name': 'ak',
            'form-0-position': 'CEO',
            'form-0-phone_number': '989119941033',
            'form-0-email': 'sepehr.ak@gmail.com',
            'form-1-first_name': 'hamed',
            'form-1-last_name': 'salimzade',
            'form-1-position': 'Manager',
            'form-1-phone_number': '989119941033',
            'form-1-email': 'hamedirani@gmail.com',
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-member',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertIn(
            b'Your team name has been set.',
            response.content
        )

        project = Project.objects.get(application = app)

        self.assertEqual(
            Team.objects.filter(project = project).count(),
            1
        )

        self.assertEqual(
            Member.objects.filter(team = project.team).count(),
            2
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )


    def test_valid_data_change_team_name(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        team = Team.objects.create(
            name = 'Iran',
            user = app.user, 
            project = app.project
        )

        data = {
            'name': 'Sage',
            'form-TOTAL_FORMS': '2',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-first_name': 'sepehr',
            'form-0-last_name': 'ak',
            'form-0-position': 'CEO',
            'form-0-phone_number': '989119941033',
            'form-0-email': 'sepehr.ak@gmail.com',
            'form-1-first_name': 'hamed',
            'form-1-last_name': 'salimzade',
            'form-1-position': 'Manager',
            'form-1-phone_number': '989119941033',
            'form-1-email': 'hamedirani@gmail.com',
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-member',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertIn(
            b'Your team name has been changed.',
            response.content
        )

        project = Project.objects.get(application = app)

        self.assertEqual(
            Team.objects.filter(project = project).count(),
            1
        )

        self.assertEqual(
            Member.objects.filter(team = project.team).count(),
            2
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )


    def test_valid_data_same_team_name(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        Team.objects.create(
            name = 'Sage',
            user = app.user, 
            project = app.project
        )

        data = {
            'name': 'Sage',
            'form-TOTAL_FORMS': '2',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-first_name': 'sepehr',
            'form-0-last_name': 'ak',
            'form-0-position': 'CEO',
            'form-0-phone_number': '989119941033',
            'form-0-email': 'sepehr.ak@gmail.com',
            'form-1-first_name': 'hamed',
            'form-1-last_name': 'salimzade',
            'form-1-position': 'Manager',
            'form-1-phone_number': '989119941033',
            'form-1-email': 'hamedirani@gmail.com',
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-member',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )

        self.assertNotIn(
            b'Your team name has been changed.',
            response.content
        )

        self.assertNotIn(
            b'Your team name has been set.',
            response.content
        )

        project = Project.objects.get(application = app)

        self.assertEqual(
            Team.objects.filter(project = project).count(),
            1
        )

        self.assertEqual(
            Member.objects.filter(team = project.team).count(),
            2
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )


class TeamPublishedViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')
        self.user.save()

        self.user.profile.email_confirmed = True
        self.user.profile.save()


    def __create_published_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.is_published = True
            self.user.profile.save()


    def __publishe_first_application(self):

        app = self.user.applications.all()[0]

        app.qatari_partnership = True
        app.kafil_fname = 'sepehr'
        app.kafil_lname = 'akbarzadeh'
        app.kafil_email = 'sepehr.ak@gmail.com'
        app.kafil_qatari_id = '1111111111111'
        app.website = 'deneb.com'
        app.industry = 'Art'
        app.deneb_result = 'Techshow'
        app.abstract = 'lorem ipsum'
        app.is_b2b = True
        app.is_b2c = False
        app.is_b2g = False
        app.is_b2b2c = False
        app.has_product = True
        app.sales_rate = '10000000'
        app.sales_rate_type = 'm'
        app.keywords = 'sepehr, akbarzadeh, abkenar, nowshahr'
        app.is_published = True

        app.save()
        return app


    def __create_published_project(self, app):
        
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.problem = 'lorem ipsum'
                app.project.solution = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = True
                
                app.project.save() 


    def __create_team(self, project):

        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': project,
            'is_published': False
        }

        self.team = Team.objects.create(**data)
    
    
    def __create_team_member(self):

        data = {
            'first_name': 'sepehr',
            'last_name': 'akbarzade',
            'position': 'CEO',
            'phone_number': '+0989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }

        self.team_member = Member.objects.create(**data)

    
    def test_without_login(self):

        self.__create_published_profile()

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        data = {
            'is_shown': True
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-published',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:project-team-published',
                    kwargs = {
                        'sku': app.sku,
                        'sku_project': app.project.sku
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )


    def test_valid_data_without_team(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        data = {
            'is_published': True
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-published',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertIn(
            b'Please Insert your team information.',
            response.content
        )


    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.__publishe_first_application()

        self.__create_published_project(app)

        self.__create_team(app.project)

        self.__create_team_member()

        data = {
            'is_published': True
        }

        response = self.client.post(
            reverse(
                'dashboard:project-team-published',
                kwargs = {
                    'sku': app.sku,
                    'sku_project': app.project.sku
                }
            ),
            data = data,
            follow = True
        )

        self.assertIn(
            b'Your team is finalized.',
            response.content
        )