from django.contrib import admin
from project.models import Application, Project
# Register your models here.
@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    pass