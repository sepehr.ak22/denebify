import re

from django import forms

from .models import Application
from .models import Project
from .models import Team
from .models import Member

from django.utils.translation import ugettext_lazy as _

from painless.regex.patterns import INTERNATIONAL_PHONE_NUMBER_PATTERN


class ApplicationDraftForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ApplicationDraftForm, self).__init__(*args, **kwargs)
        self.user = kwargs['instance']

    TRUE_FALSE_CHOICES = (
        (True, 'Yes'),
        (False, 'No')
    )

    SALE_RATE_TYPE = (
        ('m', _('Monthly')),
        ('y', _('Yearly')),
    )

    has_product = forms.ChoiceField(choices = TRUE_FALSE_CHOICES, label=_("Has Product"), 
                                    initial='', widget=forms.Select(), required=False)

    qatari_partnership = forms.ChoiceField(choices = TRUE_FALSE_CHOICES, label=_("Qatari Partnership"), 
                                           initial='', widget=forms.Select(), required=False)

    sales_rate_type = forms.ChoiceField(choices = SALE_RATE_TYPE, label=_("Sales rate Type"), 
                                           initial='', widget=forms.Select(), required=False)

    
    class Meta:
        model = Application
        fields = ('user', 'qatari_partnership', 'website',
                 'is_b2b', 'is_b2c', 'is_b2g', 'is_b2b2c', 'industry', 'has_product',
                 'sales_rate', 'deneb_result', 'is_published', 'sales_rate_type',
                 'kafil_fname', 'kafil_lname', 'kafil_email', 'kafil_qatari_id', 'keywords', 'abstract')

    
    def clean_qatari_partnership(self):

        qatari_partnership = self.cleaned_data['qatari_partnership']

        if qatari_partnership == 'True':

            return True

        if qatari_partnership == 'False':

            return False

        return qatari_partnership


    def clean_has_product(self):

        has_product = self.cleaned_data['has_product']

        if has_product == 'True':

            return True

        if has_product == 'False':

            return False

        return has_product

    
    def clean_is_b2b(self):

        is_b2b = self.cleaned_data['is_b2b']
        
        if is_b2b == 'on':

            return True

        if is_b2b == 'off':

            return False

        return is_b2b


    def clean_is_b2c(self):

        is_b2c = self.cleaned_data['is_b2c']

        if is_b2c == 'on':

            return True

        if is_b2c == 'off':

            return False

        return is_b2c

    
    def clean_is_b2g(self):

        is_b2g = self.cleaned_data['is_b2g']

        if is_b2g == 'on':

            return True

        if is_b2g == 'off':

            return False

        return is_b2g


    def clean_is_b2b2c(self):

        is_b2b2c = self.cleaned_data['is_b2b2c']

        if is_b2b2c == 'on':

            return True

        if is_b2b2c == 'off':

            return False

        return is_b2b2c
    
    
    def clean_sales_rate(self):

        sales_rate = self.cleaned_data['sales_rate']

        if sales_rate == None:

            return 0
        
        return sales_rate
    
    
    def clean(self):

        cleaned_data = super().clean()

        is_published = cleaned_data.get('is_published')

        email_confirmed = self.user.profile.email_confirmed

        # check if user is active
        if not email_confirmed:
            raise forms.ValidationError(_('Your email has not confirmed!'))

        # check if hasn't publish
        if is_published:
            raise forms.ValidationError(_('You cannot submit form when it is published already.'), code='forbidden')

        qatari_partnership = cleaned_data.get('qatari_partnership')
        kafil_fname = cleaned_data.get('kafil_fname')
        kafil_lname = cleaned_data.get('kafil_lname')
        kafil_email = cleaned_data.get('kafil_email')
        kafil_qatari_id = cleaned_data.get('kafil_qatari_id')
        # import pdb ; pdb.set_trace()
        if qatari_partnership:
            if not kafil_fname:
                raise forms.ValidationError(_('Please enter kafil first name.'), code='forbidden')
            if not kafil_lname:
                raise forms.ValidationError(_('Please enter kafil last name.'), code='forbidden')
            if not kafil_email:
                raise forms.ValidationError(_('Please enter kafil email address.'), code='forbidden')
            if not kafil_qatari_id:
                raise forms.ValidationError(_('Please enter kafil qatari id.'), code='forbidden')
        
        else:

            if kafil_fname or kafil_lname or kafil_email or kafil_qatari_id:
                
                raise forms.ValidationError(
                    _('You must select True for Qatari Partnership if you have a kafil.'),
                    code='forbidden'
                )
        
        keywords = cleaned_data.get('keywords')
        if keywords:

            if not ',' in keywords:
                raise forms.ValidationError(_("Please seperate keywords with `,`."))
            else:
                keywords = [s.strip() for s in keywords.split(',')]
                if len(keywords) < 3:
                    raise forms.ValidationError(_("Please enter at least 4 keywords."))
                elif len(keywords) > 15:
                    raise forms.ValidationError(_("Please enter fewer keywords. Maximum range of keywords are 14."))
                else:
                    pass

        return cleaned_data


class ApplicationPublishForm(forms.ModelForm):

    class Meta:
        
        model = Application

        fields = ('is_published',)

        widgets = {
            'is_published': forms.HiddenInput(),
        }

    def clean(self):

        cleaned_data = super().clean()

        app = self.instance
        
        if not app.user.profile.is_published:

            raise forms.ValidationError(_('You must finalize your profile.'), code='forbidden')

        if not app.is_b2b:
            if not app.is_b2c:
                if not app.is_b2g:
                    if not app.is_b2b2c:
                        raise forms.ValidationError(_('Please select at least one of business models.'), code='forbidden')
        
        if not app.industry:
            raise forms.ValidationError(_('Please fill industry field.'), code='forbidden')

        if not app.deneb_result:
            raise forms.ValidationError(_('Please fill deneb_result field.'), code='forbidden')

        if not app.abstract:
            raise forms.ValidationError(_('Please fill abstract field.'), code='forbidden')

        if not app.sales_rate:
            raise forms.ValidationError(_('Please fill sales_rate field.'), code='forbidden')
        
        if not app.sales_rate_type:
            raise forms.ValidationError(_('Please fill sales_rate_type field.'), code='forbidden')

        if not app.keywords:
            raise forms.ValidationError(_('Please fill keywords field.'), code='forbidden')
        
        if app.sales_rate == 0:
            raise forms.ValidationError(_('sales_rate value can not be 0.'), code='forbidden')


class ProjectDraftForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        self.user = kwargs.pop('user', None)

        super(ProjectDraftForm, self).__init__(*args, **kwargs)


    class Meta:

        model = Project

        fields = ('title',
                  'cover',
                  'video',
                  'summary', 'summary_img',
                  'problem', 'problem_img',
                  'solution', 'solution_img',
                  'product', 'product_img',
                  'customer', 'customer_img',
                  'business', 'business_img',
                  'market', 'market_img',
                  'competition', 'competition_img',
                  'vision', 'vision_img',
                  'founders', 'founders_img',
                 )


    def clean_summary(self):

        summary = self.cleaned_data['summary']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in summary:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return summary


    def clean_problem(self):

        problem = self.cleaned_data['problem']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in problem:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return problem


    def clean_solution(self):

        solution = self.cleaned_data['solution']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in solution:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return solution


    def clean_Product(self):

        Product = self.cleaned_data['Product']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Product:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Product


    def clean_Customer(self):

        Customer = self.cleaned_data['Customer']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Customer:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Customer


    def clean_Business(self):

        Business = self.cleaned_data['Business']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Business:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Business


    def clean_Market(self):

        Market = self.cleaned_data['Market']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Market:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Market


    def clean_Competition(self):

        Competition = self.cleaned_data['Competition']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Competition:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Competition


    def clean_Vision(self):

        Vision = self.cleaned_data['Vision']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Vision:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Vision


    def clean_Founders(self):

        Founders = self.cleaned_data['Founders']

        except_values = ['<', '>', '/']

        for e_v in except_values:
        
            if e_v in Founders:

                raise forms.ValidationError(
                    _('You can\'t use from < > / characters.'),
                    code='forbidden'
                )

        return Founders
    
    
    def clean(self):
        cleaned_data = super().clean()
        is_published = cleaned_data.get('is_published')
        # import pdb ; pdb.set_trace()
        email_confirmed = self.user.profile.email_confirmed

        # check if user is active
        if not email_confirmed:
            raise forms.ValidationError(_('Your email has not confirmed!'))

        # check if hasn't publish
        if is_published:
            raise forms.ValidationError(_('You cannot submit form when it is published already.'), code='forbidden')


class ProjectPublishForm(forms.ModelForm):
    
    class Meta:

        model = Project

        fields = ('is_published',)

        widgets = {
            'is_published': forms.HiddenInput(),
        }


    def clean(self):

        cleaned_data = super().clean()

        project = self.instance

        if not project.application.user.profile.email_confirmed:

            raise forms.ValidationError(_('Your email has not confirmed!'))

        if not project.application.user.profile.is_published:

            raise forms.ValidationError(_('You must finalize your profile.'), code='forbidden')

        if not project.title:

            raise forms.ValidationError(
                'Please fill the title field',
                code = 'forbidden'
            )

        if not project.summary:

            raise forms.ValidationError(
                'Please fill the summary field',
                code = 'forbidden'
            )

        if not project.problem:

            raise forms.ValidationError(
                'Please fill the problem field',
                code = 'forbidden'
            )

        if not project.solution:

            raise forms.ValidationError(
                'Please fill the solution field',
                code = 'forbidden'
            )

        if not project.product:

            raise forms.ValidationError(
                'Please fill the product field',
                code = 'forbidden'
            )

        if not project.customer:

            raise forms.ValidationError(
                'Please fill the customer field',
                code = 'forbidden'
            )

        if not project.business:

            raise forms.ValidationError(
                'Please fill the business field',
                code = 'forbidden'
            )

        if not project.market:

            raise forms.ValidationError(
                'Please fill the market field',
                code = 'forbidden'
            )

        if not project.vision:

            raise forms.ValidationError(
                'Please fill the vision field',
                code = 'forbidden'
            )

        if not project.founders:

            raise forms.ValidationError(
                'Please fill the founders field',
                code = 'forbidden'
            )

        if not project.cover:

            raise forms.ValidationError(
                'Please fill the cover field',
                code = 'forbidden'
            )

        if not project.video:

            raise forms.ValidationError(
                'Please fill the video field',
                code = 'forbidden'
            )

        if not project.competition:

            raise forms.ValidationError(
                'Please fill the competition field',
                code = 'forbidden'
            )


class ProjectShownForm(forms.ModelForm):

    
    class Meta:

        model = Project

        fields = ('is_shown',)

        widgets = {
            'is_shown': forms.HiddenInput(),
        }

    
    def clean(self):

        cleaned_data = super().clean()

        project = self.instance

        if not project.application.user.profile.email_confirmed:

            raise forms.ValidationError(_('Your email has not confirmed!'))

        if not project.application.user.profile.is_published:

            raise forms.ValidationError(_('You must finalize your profile.'), code='forbidden')

        if not project.is_published:

            raise forms.ValidationError(
                'At the first you must published your project.',
                code = 'forbidden'
            )

        if not project.team.is_published:

            raise forms.ValidationError(
                'You must finalize your project team.',
                code = 'forbidden'
            )


class TeamDraftForm(forms.ModelForm):


    def __init__(self, *args, **kwargs):
        super(TeamDraftForm, self).__init__(*args, **kwargs)
        self.user = kwargs['instance']


    class Meta:

        model = Team

        fields = ('name',)
    
    def clean_name(self):

        name = self.cleaned_data['name']

        if not name:

            raise forms.ValidationError(
                _('Please choose a name for your team.'),
                code='forbidden'
            )

        return name


    def clean(self):

        cleaned_data = super().clean()

        email_confirmed = self.user.profile.email_confirmed

        if not email_confirmed:
            raise forms.ValidationError(_('Your email has not confirmed!'))

        # check if hasn't publish
        if self.user.profile.is_published:

            raise forms.ValidationError(_('You must finalize your profile.'), code='forbidden')


class TeamPublishedForm(forms.ModelForm):

    class Meta:

        model = Team

        fields = ('is_published',)

        widgets = {
            'is_published': forms.HiddenInput(),
        }

    def clean(self):

        team = self.instance

        cleaned_data = super().clean()
        # import pdb ; pdb.set_trace()

        if not team.project.is_published:

            raise forms.ValidationError(
                _('You must finalize your project.'),
                code='invalid'
            )
        
        if team.is_published:

            raise forms.ValidationError(
                _('You cannot submit form when it is published already.'),
                code='invalid'
            )

        is_published = cleaned_data.get('is_published')

        if not is_published:

            raise forms.ValidationError(
                _('Not valid request'),
                code='invalid'
            )

        colleagues = team.colleagues.all()

        if not colleagues:

            raise forms.ValidationError(
                _('You cannot publish it unless your team has members.'),
                code='invalid'
            )


class TeamMemberForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.team = kwargs.pop('team', None)
        super(TeamMemberForm, self).__init__(*args, **kwargs)

    class Meta:

        model = Member

        fields = ('first_name', 'last_name',
                  'position', 'phone_number', 'email',
                 )

    
    def clean_phone_number(self):

        phone_number = self.cleaned_data['phone_number']

        if phone_number:

            if not re.match(INTERNATIONAL_PHONE_NUMBER_PATTERN, phone_number):

                raise forms.ValidationError(_('Invalid value: %(value)s'), code='invalid', params={'value': phone_number})

        return phone_number