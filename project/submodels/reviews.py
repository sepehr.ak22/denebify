from django.db import models

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _


class Reviews(models.Model):
    message = models.TextField(null = True, max_length=526)
    submited_by = models.ForeignKey(User, on_delete = models.SET_NULL, null = True)

    is_shown = models.BooleanField(default = True)

    project = models.ForeignKey('Project', related_name='comments', on_delete = models.SET_NULL , null =True)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')

    def __str__(self):
        return self.project

    # def get_absolute_url(self):
    #     return reverse("project:comment", kwargs={"slug": self.slug})