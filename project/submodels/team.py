from django.db import models

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _


class Team(models.Model):
    name = models.CharField(_("Team Name"), max_length = 200)
    user = models.ForeignKey(User, related_name='teams', on_delete=models.CASCADE)
    project = models.OneToOneField('project.Project', related_name='team', on_delete=models.CASCADE,)
    is_published = models.BooleanField(_("Is published"), default=False)

    class Meta:
        """Meta definition for Profile."""
        verbose_name = _('Team')
        verbose_name_plural = _('Teams')

    def get_full_name(self):
        return '{}'.format(self.name)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Team, self).save(*args, **kwargs)