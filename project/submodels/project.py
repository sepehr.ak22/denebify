import secrets

from django.db import models

from django.utils.translation import ugettext_lazy as _

from django.core.validators import (
    MaxLengthValidator,
    MinLengthValidator,
    FileExtensionValidator
)

from django.urls import reverse

from django.utils.text import slugify

from markdown_deux.templatetags.markdown_deux_tags import markdown_allowed

from django.contrib.contenttypes.fields import GenericRelation

from hitcount.models import HitCountMixin, HitCount

from painless.upload_to import user_directory_path_project

from painless.validators import FileSize

from painless.validators import ExtensionValidator

from project.managers import ProjectManager


class Project(models.Model, HitCountMixin):
    
    application = models.OneToOneField('Application',
                                       verbose_name = _("Application"), 
                                       related_name = 'project',
                                       on_delete = models.PROTECT,
                                       blank = True, null = True
                                      )

    sku = models.CharField(max_length = 256, editable = False, unique = True,null=True)

    title = models.CharField(_("Title"), max_length=60, validators=[MaxLengthValidator(60)], blank = True, null = True)
    slug = models.SlugField(_("Slug"), max_length=60, validators=[MaxLengthValidator(60)], editable = False, allow_unicode=True, blank = True, null = True)

    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    cover = models.ImageField(_("Cover"), 
                                upload_to=user_directory_path_project,
                                height_field='height_field', 
                                width_field='width_field', 
                                max_length=100, 
                                blank = True, null = True,
                                validators=[FileExtensionValidator(['jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG'])]
                                )

    video = models.FileField(_("Video"),
                               upload_to=user_directory_path_project,
                               null = True, blank = True, 
                               validators=[FileExtensionValidator(['mp4']), FileSize],
                              )

    is_shown = models.BooleanField(default = False)
    
    summary_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    summary_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    summary_img = models.ImageField(_("Summary"), upload_to=user_directory_path_project, height_field='summary_height_field', width_field='summary_width_field', max_length=100, blank = True, null = True)
    summary = models.TextField(_("Summary"), blank = True, null = True, help_text=markdown_allowed())

    problem_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    problem_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    problem_img = models.ImageField(_("Problem"), upload_to=user_directory_path_project, height_field='problem_height_field', width_field='problem_width_field', max_length=100, blank = True, null = True)
    problem = models.TextField(_("Problem"), blank = True, null = True, help_text=markdown_allowed())

    solution_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    solution_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    solution_img = models.ImageField(_("Solution"), upload_to=user_directory_path_project, height_field='solution_height_field', width_field='solution_width_field', max_length=100, blank = True, null = True)
    solution = models.TextField(_("Solution"), blank = True, null = True, help_text=markdown_allowed())

    product_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    product_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    product_img = models.ImageField(_("Product"), upload_to=user_directory_path_project, height_field='product_height_field', width_field='product_width_field', max_length=100, blank = True, null = True)
    product = models.TextField(_("Product"), blank = True, null = True, help_text=markdown_allowed())

    customer_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    customer_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    customer_img = models.ImageField(_("Customer"), upload_to=user_directory_path_project, height_field='customer_height_field', width_field='customer_width_field', max_length=100, blank = True, null = True)
    customer = models.TextField(_("Customer"), blank = True, null = True, help_text=markdown_allowed())

    business_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    business_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    business_img = models.ImageField(_("Business"), upload_to=user_directory_path_project, height_field='business_height_field', width_field='business_width_field', max_length=100, blank = True, null = True)
    business = models.TextField(_("Business"), blank = True, null = True, help_text=markdown_allowed())

    market_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    market_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    market_img = models.ImageField(_("Market"), upload_to=user_directory_path_project, height_field='market_height_field', width_field='market_width_field', max_length=100, blank = True, null = True)
    market = models.TextField(_("Market"), blank = True, null = True, help_text=markdown_allowed())

    competition_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    competition_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    competition_img = models.ImageField(_("Competition"), upload_to=user_directory_path_project, height_field='competition_height_field', width_field='competition_width_field', max_length=100, blank = True, null = True)
    competition = models.TextField(_("Competition"), blank = True, null = True, help_text=markdown_allowed())

    vision_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    vision_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    vision_img = models.ImageField(_("Vision"), upload_to=user_directory_path_project, height_field='vision_height_field', width_field='vision_width_field', max_length=100, blank = True, null = True)
    vision = models.TextField(_("Vision"), blank = True, null = True, help_text=markdown_allowed())

    founders_width_field = models.PositiveIntegerField(_("Width Field"), editable = False, blank = True, null = True)
    founders_height_field = models.PositiveIntegerField(_("Height Field"), editable = False, blank = True, null = True)
    founders_img = models.ImageField(_("Founders"), upload_to=user_directory_path_project, height_field='founders_height_field', width_field='founders_width_field', max_length=100, blank = True, null = True)
    founders = models.TextField(_("Founders"), blank = True, null = True, help_text=markdown_allowed())

    is_published = models.BooleanField(default = False)

    hit_count_generic = GenericRelation(HitCount, object_id_field='object_pk', related_query_name='hit_count_generic_relation')

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = ProjectManager()


    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')
        index_together = ('slug', 'is_published', 'is_shown')

    def __str__(self):
            return '{}:{}'.format(self.application, self.title) if self.title else 'new project'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)

        if not self.pk:
            self.sku = secrets.token_hex(16)

        super(Project, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('pages:project-detail', kwargs={'slug': self.slug})