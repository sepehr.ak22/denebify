import secrets

from django.db import models

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from django.utils.text import slugify

from django.core.validators import (
    MaxLengthValidator,
    MinLengthValidator,
    EmailValidator,
    URLValidator,
    RegexValidator
)

from project.submanagers.application import ApplicationManager


class Application(models.Model):

    INDUSTRIES = (
        ('Art', 'Art'),
        ('Aerospace industry', 'Aerospace industry'),
        ('Agriculture', 'Agriculture'),
        ('Fishing', 'Fishing'),
        ('Chemical', 'Chemical'),
        ('Computer', 'Computer'),
        ('Construction', 'Construction'),
        ('Defense', 'Defense'),
        ('Education', 'Education'),
        ('Entertainment', 'Entertainment'),
        ('Financial', 'Financial'),
        ('Food', 'Food'),
        ('Healthcare', 'Healthcare'),
        ('Hospitality', 'Hospitality'),
        ('Manufacturing', 'Manufacturing'),
        ('Media', 'Media'),
        ('Telecom', 'Telecom'),
        ('Transport', 'Transport'),
        ('Water', 'Water'),
        ('Sports', 'Sports'),
        ('E-Commerce', 'E-Commerce'),
        ('Tourism', 'Tourism'),
        ('Fashion', 'Fashion'),
        ('Marketing', 'Marketing'),
        ('HR', 'HR'),
    )

    deneb_resultS = (
        ('No Experience', _('No Experience')),
        ('Rejection', _('Rejection')),
        ('Incubation', _('Incubation')),
        ('Services', _('Services')),
        ('Techshow', _('Techshow')),
        ('Referal to other organization', _('Referal to other organization')),
    )

    SALE_RATE_TYPE = (
        ('m', _('Monthly')),
        ('y', _('Yearly')),
    )

    sku = models.CharField(max_length = 256, editable = False, unique = True)
    user = models.ForeignKey(User, related_name='applications', on_delete=models.CASCADE, null=True, blank=True)

    qatari_partnership = models.BooleanField(_("Qatari Partnership"), default = False, null=True, blank=True)
    
    # Kafil information if qatari partnership is true
    kafil_fname = models.CharField(_("Kafil First Name"), validators=[MaxLengthValidator(13), MinLengthValidator(12)], max_length = 45, null=True, blank=True)
    kafil_lname = models.CharField(_("Kafil Last Name"), validators=[MaxLengthValidator(13), MinLengthValidator(12)], max_length = 45, null=True, blank=True)
    kafil_email = models.EmailField(_("Kafil Email"), validators=[EmailValidator], max_length=254, null=True, blank=True)
    kafil_qatari_id = models.CharField(_("Qatari iD"), max_length = 60,
                        validators=[RegexValidator(r'^[0-9]*$', 'Only positive integer are allowed.')], 
                        null=True, blank=True
                    )
 
    website = models.URLField(_("Website"), max_length=200, validators=[URLValidator], null=True, blank=True)
    abstract  = models.TextField(_("Abstract"), null=True, blank=True)

    is_b2b = models.BooleanField(_("Is B2B"), default = False)
    is_b2c = models.BooleanField(_("Is B2C"), default = False)
    is_b2g = models.BooleanField(_("Is B2G"), default = False)
    is_b2b2c = models.BooleanField(_("Is B2B2C"), default = False)

    industry = models.CharField(_("Industry"), choices = INDUSTRIES, max_length = 30, null=True, blank=True)
    has_product = models.BooleanField(_("Has Product"), default = False)

    sales_rate = models.PositiveIntegerField(_("Sales rate"), null=True, blank=True)
    sales_rate_type = models.CharField(_("Sales rate Type"), max_length=1, choices=SALE_RATE_TYPE, null=True, blank=True)

    deneb_result = models.CharField(_("Deneb Result"), choices=deneb_resultS, max_length = 35, null=True, blank=True)

    keywords = models.CharField(_("Keywords"), max_length = 120, null = True, blank = True)
    
    is_published = models.BooleanField(_("Finalize"), default = False)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = ApplicationManager()

    class Meta:
        """Meta definition for Experience."""

        verbose_name = _('Application')
        verbose_name_plural = _('Applications')
        index_together = ('sku', 'is_published')

    def __str__(self):
        return self.sku
    
    def save(self, *args, **kwargs):
        if not self.pk:
            self.sku = secrets.token_hex(16)
        super(Application, self).save(*args, **kwargs)