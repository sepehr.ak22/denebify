from django.db import models

from django.utils.translation import ugettext_lazy as _


class Member(models.Model):
    first_name = models.CharField(_("First Name"), max_length = 100)
    last_name = models.CharField(_("Last Name"), max_length = 100)
    position = models.CharField(_("Position"), max_length = 100)
    phone_number = models.CharField(_("Phone Number"), max_length = 20, null=True, blank=True)
    email = models.EmailField(_('email address'), blank=True)

    team = models.ForeignKey('Team', related_name='colleagues', on_delete=models.CASCADE)


    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    
    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return self.first_name

    def save(self, *args, **kwargs):
        super(Member, self).save(*args, **kwargs)