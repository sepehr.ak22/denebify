from django.db import models


class ApplicationQuerySet(models.QuerySet):

    def applications_count(self, user):

        return self.filter(user = user).count()


class ApplicationManager(models.Manager):

    def __get_applications_progress(self, app):
        
        application_progress_num = 10

        attrs = vars(app)

        for key in attrs.keys():

            if key in [
                'qatari_partnership',
                'website',
                'abstract',
                'industry',
                'sales_rate',
                'sales_rate_type',
                'deneb_result',
                'keywords',
                'is_published',
            ]:
                if not attrs[key]:

                    application_progress_num -= 1

            else:

                continue
        
        if not (attrs['is_b2b'] or attrs['is_b2c'] or attrs['is_b2g'] or attrs['is_b2b2c']):

            application_progress_num -= 1

        percent_complete_of_application = (application_progress_num / 10) * 100
      
        return attrs['sku'], percent_complete_of_application


    def get_queryset(self):

        return ApplicationQuerySet(self.model, using = self._db)  # Important!


    def applications_count(self, user):

        return self.get_queryset().applications_count(user)


    # Returns list of the tuples that contain SKU of application and percentage of completed application fields
    def applications_progress(self, user):

        app = self.get_queryset().filter(user = user)

        return list(map(lambda app: self.__get_applications_progress(app), app))