from django.db import models

from project.submodels.application import Application


class ProjectQuerySet(models.QuerySet):

    def projects(self, user):

        apps = Application.objects.filter(user = user)

        return self.filter(application__in = apps)

    
class ProjectManager(models.Manager):

    def __get_project_status(self, project):

        project_progress_num = 15

        attrs = vars(project)

        for key in attrs.keys():

            if key in ['is_published',
                       'is_shown',
                       'title',
                       'summary',
                       'problem',
                       'solution',
                       'product',
                       'customer',
                       'business',
                       'market',
                       'competition',
                       'vision',
                       'founders',
                       'cover',
                       'video']:

                if not attrs[key]:

                    project_progress_num -= 1

            else:

                continue

        percent_complete_of_project = (project_progress_num / 15) * 100
      
        return attrs['sku'], percent_complete_of_project


    def get_queryset(self):

        return ProjectQuerySet(self.model, using = self._db)  # Important!


    def projects_count(self, user):

        return self.projects(user).count()


    def projects(self, user):

        return self.get_queryset().projects(user)

    
    # Returns list of the tuples that contain SKU of project and percentage of completed project fields
    def projects_progress(self, user):

        projects = self.projects(user)

        return list(map(lambda project: self.__get_project_status(project), projects))