from django.views import View

from django.views.generic import ListView

from django.contrib import messages

from django.shortcuts import (
    redirect,
    render,
    get_object_or_404
)

from django.forms.models import model_to_dict

from django.contrib.auth.mixins import LoginRequiredMixin

from accounts.forms import ProfileDraftForm

from project.models import (
    Application,
    Project,
    Team
)

from project.forms import (
    ApplicationDraftForm,
    ProjectDraftForm,
    ApplicationPublishForm
)

from step.models import Step


class ApplicationCreateView(LoginRequiredMixin, View):
    
    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }
        
        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False
    
    
    def is_profile_published(self):

        return True if self.request.user.profile.is_published else False
    

    def post(self, request, pk=None, *args, **kwargs):

        if self.is_profile_published():

            # if self.recaptcha_validation():

                app = Application.objects.create(user = request.user)
    
                return redirect('dashboard:application-edit', app.sku)
            
            # else:

            #     messages.error(request, 'Captcha Failure')

        else:

            messages.error(request, 'You must first finalize your profile')

        return redirect('dashboard:application-list')


class ApplicationListView(LoginRequiredMixin, ListView):

    model = Application

    context_object_name = 'applications'

    template_name = 'dashboard/pages/application-list.html'

    page_name = 'Application'


class ApplicationView(LoginRequiredMixin, View):

    template_name = 'dashboard/pages/application-process.html'
    view = {
        'page_name': "Application"
    }
    form = ApplicationDraftForm

    
    def is_user_valid(self, request, instance, *args, **kwargs):

        return True if request.user == instance.user else False
    

    def find_project(self, request, instance, *args, **kwargs):
        
        try:

            project = get_object_or_404(Project, sku = instance.project.sku)

            form = ProjectDraftForm(initial = model_to_dict(project))

        except:

            project = None

            form = None

        return project, form

    
    def find_team(self, project):

        try:

            team = Team.objects.get(project = project)

        except:

            team = None
        
        return team
    
    
    def current_step(self, app):

        last_step = Step.objects.filter(application = app).filter(state='c').last()

        if last_step:

            current_step = 5 if int(last_step.step) == 6 else int(last_step.step)

        else:

            if app.user.profile.is_published:

                current_step = 1

            else:

                current_step = 0

        return current_step

    
    def get(self, request, sku=None, *args, **kwargs):
        
        app = get_object_or_404(Application, sku = sku)

        current_step = self.current_step(app)

        if self.is_user_valid(request, app):
            
            project, project_form = self.find_project(request, app)
            
            team = self.find_team(project)
            
            profile_form = ProfileDraftForm(
                initial = model_to_dict(request.user.profile),
                instance = self.request.user
            )

            form = self.form(initial=model_to_dict(app), instance=request.user)
            
            return render(
                request,
                self.template_name,
                {
                    "form": form, 
                    "view": self.view, 
                    "application": app, 
                    'current_step': current_step,
                    'project': project,
                    'project_form': project_form,
                    'team': team,
                    'profile_form': profile_form
                }
            )

        else:

            messages.error(request, "Not valid request.")
            
            return redirect('dashboard:application-list', {'current_step': current_step})

        
    def post(self, request, sku=None, *args, **kwargs):
        
        app = get_object_or_404(Application, sku = sku)

        form = self.form(request.POST, instance=request.user)
        
        if self.is_user_valid(request, app):

            if form.is_valid():

                form.save(commit=False)
                
                app.qatari_partnership = form.cleaned_data['qatari_partnership']

                app.website = form.cleaned_data['website']

                app.abstract = form.cleaned_data['abstract']

                app.keywords = form.cleaned_data['keywords']

                app.industry = form.cleaned_data['industry']

                app.deneb_result = form.cleaned_data['deneb_result']

                app.is_b2b = form.cleaned_data['is_b2b']

                app.is_b2c = form.cleaned_data['is_b2c']

                app.is_b2g = form.cleaned_data['is_b2g']

                app.is_b2b2c = form.cleaned_data['is_b2b2c']

                app.has_product = form.cleaned_data['has_product']

                app.sales_rate = form.cleaned_data['sales_rate']

                app.sales_rate_type = form.cleaned_data['sales_rate_type']

                app.kafil_fname = form.cleaned_data['kafil_fname']
                
                app.kafil_lname = form.cleaned_data['kafil_lname']
                
                app.kafil_email = form.cleaned_data['kafil_email']
                
                app.kafil_qatari_id = form.cleaned_data['kafil_qatari_id']

                app.save()
                
                messages.success(request, "Your application has been changed.")

                return redirect('dashboard:application-edit', app.sku)

            else:

                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   
                    
                    messages.error(request, '{}: {}'.format(field, message))

                return redirect('dashboard:application-edit', app.sku,)
        
        else:

            messages.error(request, "Not valid request.")

            
            return redirect('dashboard:application-edit', app.sku)


class ApplicationFinalizeView(LoginRequiredMixin, View):

    view = {
        'page_name': "Application"
    }
    form = ApplicationPublishForm


    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False


    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.user else False


    def get(self, request, sku, *args, **kwargs):
        return redirect("dashboard:home")


    def post(self, request, sku, *args, **kwargs):
        # import pdb ; pdb.set_trace()
        application = get_object_or_404(Application, sku = sku)

        if self.is_user_valid(request, application):

            # if self.recaptcha_validation():

                form = self.form(request.POST, instance=application)

                if form.is_valid():

                    application.is_published = True

                    application.save()

                    messages.success(request, "Your application is finalized.")

                    return redirect("dashboard:application-edit", sku)

                else:

                    for f, m in form.errors.get_json_data().items():

                        message = m[0]['message']

                        field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   
                        
                        messages.error(request, '{}: {}'.format(field, message))

            # else:

            #     messages.error(request, "Captch Failure.")
        
        else:

                messages.error(request, "Not valid request.")
        
        return redirect("dashboard:application-edit", sku)