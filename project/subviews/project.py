from django.views.generic import View

from django.shortcuts import (
    render,
    redirect,
)

from django.http import Http404

from django.contrib import messages

from django.forms.models import model_to_dict

from django.shortcuts import get_object_or_404

from django.contrib.auth.mixins import LoginRequiredMixin

from project.models import Project

from project.forms import (
    ProjectDraftForm,
    ProjectPublishForm,
    ProjectShownForm,
    TeamMemberForm
)


class ProjectTemplateView(LoginRequiredMixin, View):

    template_name = "dashboard/pages/project.html"

    view = {
        'page_name': 'Application',
        'remove_footer': True
    }

    def get(self, request, sku, sku_project, *args, **kwargs):

        project = get_object_or_404(Project, sku=sku_project)

        return render(
            request,
            self.template_name, {
                'project': project,
                'view': self.view
            }
        )


class ProjectView(LoginRequiredMixin, View):

    template_name = 'dashboard/pages/application-project-edit.html'
    view = {
        'page_name': "Project"
    }
    form = ProjectDraftForm
    
    def is_user_valid(self, request, instance, *args, **kwargs):

        return True if request.user == instance.application.user else False


    def get(self, request, sku=None, sku_project=None, *args, **kwargs):
        
        project = get_object_or_404(Project, sku = sku_project)
        
        if self.is_user_valid(request, project):

            form = self.form(initial = model_to_dict(project))

            return render(
                request,
                self.template_name, {
                    "form": form,
                    "view": self.view,
                    "project": project 
                }
            )
        
        else:

            raise Http404


    def post(self, request, sku=None, sku_project=None, *args, **kwargs):

        project = get_object_or_404(Project, sku=sku_project)

        form = self.form(request.POST, request.FILES, instance=project, user=request.user)

        if self.is_user_valid(self.request, project):

            if form.is_valid():
                
                form.save(commit=False)

                project.title = form.cleaned_data.get('title')

                project.summary = form.cleaned_data.get('summary')

                project.summary_img = form.cleaned_data.get('summary_img')

                project.problem = form.cleaned_data.get('problem')

                project.problem_img = form.cleaned_data.get('problem_img')

                project.solution = form.cleaned_data.get('solution')

                project.solution_img = form.cleaned_data.get('solution_img')

                project.Product = form.cleaned_data.get('Product')

                project.product_img = form.cleaned_data.get('product_img')

                project.Customer = form.cleaned_data.get('Customer')

                project.customer_img = form.cleaned_data.get('customer_img')

                project.Business = form.cleaned_data.get('Business')

                project.business_img = form.cleaned_data.get('business_img')

                project.Market = form.cleaned_data.get('Market')

                project.market_img = form.cleaned_data.get('market_img')

                project.Competition = form.cleaned_data.get('Competition')

                project.competition_img = form.cleaned_data.get('competition_img')

                project.Vision = form.cleaned_data.get('Vision')

                project.vision_img = form.cleaned_data.get('vision_img')

                project.Founders = form.cleaned_data.get('Founders')

                project.founders_img = form.cleaned_data.get('founders_img')

                project.video = form.cleaned_data.get('video')

                project.cover = form.cleaned_data.get('cover')

                project.is_shown = False

                project.save()
                
                messages.success(request, "Your project has been changed.")
                
                return redirect('dashboard:application-edit', project.application.sku)

            else:

                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                    messages.error(request, '{}: {}'.format(field, message))

                return redirect("dashboard:application-edit", project.application.sku)

        else:

            messages.error(request, "Some Error has been occured.")

            return redirect('dashboard:application-edit', project.application.sku)


class ProjectFinalizeView(LoginRequiredMixin, View):

    view = {
        'page_name': "Project"
    }
    form = ProjectPublishForm

    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.user else False


    def post(self, request, sku, sku_project, *args, **kwargs):

        project = Project.objects.get(sku = sku_project)

        form = self.form(request.POST, instance=project)

        
        if self.is_user_valid(request, project.application):

            if form.is_valid():

                messages.success(request, "Your project is finalized.")

                project.is_published = True
                project.save()

                return redirect("dashboard:application-edit", project.application.sku)

            else:
                
                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                    messages.error(request, '{}: {}'.format(field, message))

                return redirect("dashboard:application-edit", project.application.sku)

        else:

            raise Http404

        return redirect("dashboard:application-edit", project.application.sku)


class ProjectShownView(LoginRequiredMixin, View):

    view = {
        'page_name': "Project"
    }

    form = ProjectShownForm

    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.application.user else False

    def post(self, request, sku, sku_project, *args, **kwargs):

        project = get_object_or_404(Project, sku = sku_project)

        form = self.form(request.POST, instance=project)

        if self.is_user_valid(request, project):

            if form.is_valid():

                project.is_shown = True

                project.save()

                messages.info(request, 'Your project has been shown in site.')

                return redirect("dashboard:application-edit", project.application.sku)
            
            else:
                
                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                    messages.error(request, '{}: {}'.format(field, message))

                return redirect("dashboard:application-edit", project.application.sku)

        else:

            raise Http404

        return redirect("dashboard:application-edit", project.application.sku)