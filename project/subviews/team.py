from django.views.generic import View

from django.shortcuts import redirect

from django.forms import (
    formset_factory,
    inlineformset_factory
)

from django.contrib import messages

from django.shortcuts import get_object_or_404

from django.contrib.auth.mixins import LoginRequiredMixin

from project.forms import (
    TeamMemberForm,
    TeamDraftForm,
    TeamPublishedForm
)

from project.models import (
    Project,
    Team,
    Member
)


class TeamMemberView(LoginRequiredMixin, View):

    view = {
        'page_name': "TeamMember"
    }
    form = TeamMemberForm


    def recaptcha_validation(self):

        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False


    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.application.user else False


    def post(self, request, sku_project, *args, **kwargs):
        project = get_object_or_404(Project, sku = sku_project)

        if self.is_user_valid(request, project):
            
            team_form = TeamDraftForm(request.POST, instance = request.user)
            
            if team_form.is_valid():

                if not Team.objects.filter(project = project).exists():
                    
                    team = Team.objects.create(
                        name = team_form.cleaned_data['name'],
                        user = request.user, 
                        project = project
                    )

                    messages.success(request, "Your team name has been set.")
                    
                else:

                    if project.team.name != team_form.cleaned_data.get('name'):

                        project.team.name = team_form.cleaned_data.get('name')

                        project.team.save()

                        messages.success(request, "Your team name has been changed.")
                    
                    team = project.team

                MemberFormSet = formset_factory(
                    TeamMemberForm,
                    can_delete = True
                )

                member_formset = MemberFormSet(request.POST)

                if member_formset.is_valid():
                    
                    for form in member_formset:

                        instance = form.save(commit = False)

                        instance.team = team

                        instance.save()

                    return redirect('dashboard:application-edit', project.application.sku)

                else:

                    return redirect('dashboard:application-edit', project.application.sku)
                
            else:
                
                return redirect('dashboard:application-edit', project.application.sku)

        else:

            messages.error(request, "Not valid request")

            return redirect('dashboard:application-edit', project.application.sku)


class TeamPublishedView(LoginRequiredMixin, View):

    view = {
        'page_name': "Team"
    }
    form = TeamPublishedForm


    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False

    
    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.project.application.user else False
    

    def post(self, request, sku_project, *args, **kwargs):

        project = get_object_or_404(Project, sku = sku_project)

        # if self.recaptcha_validation():

        try:

            team = Team.objects.get(project = project)

        except:

            messages.error(request, "Please Insert your team information.")

            return redirect('dashboard:application-edit', project.application.sku)

        if self.is_user_valid(request, team):
        
            form = self.form(request.POST, instance=team)

            if form.is_valid():

                team.is_published = form.cleaned_data['is_published']

                team.save()
                
                messages.success(request, "Your team is finalized.")

                return redirect('dashboard:application-edit', project.application.sku)

        else:

            messages.error(request, "Not valid request.")

        # else:

        #     messages.error(request, "Captcha Failure.")

        return redirect('dashboard:application-edit', project.application.sku)