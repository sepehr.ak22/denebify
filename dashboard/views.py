from django.views.generic import TemplateView

from django.contrib.auth.mixins import LoginRequiredMixin

from accounts.models import Profile

from project.models import (
    Application,
    Project
)

from ticket.models import Ticket

from step.models import Step


class HomeView(LoginRequiredMixin, TemplateView):

    template_name = "dashboard/pages/home.html"

    page_name = 'Dashboard'


    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)

        context['profile_progress'] = Profile.objects.profile_progress(self.request.user)

        context['profile_progress_num'] = 11

        # Returns False if the profile is in progress otherwise True
        context['profile_status'] = Step.objects.profile_status(self.request.user)

        context["apps"] = Application.objects.filter(user = self.request.user)

        context['applications_count'] = Application.objects.applications_count(self.request.user)

        context['completed_applications_count'] = Step.objects.completed_applications_count(self.request.user)

        context['percentage_of_completed_applications'] = Step.objects.percentage_of_completed_applications(self.request.user)

        # Returns list of the tuples that contain SKU of application and percentage of completed application fields
        context['applications_progress'] = Application.objects.applications_progress(self.request.user)
        
        context["projects"] = Project.objects.projects(self.request.user)

        context["projects_count"] = Project.objects.projects_count(self.request.user)

        context['completed_projects_count'] = Step.objects.completed_projects_count(self.request.user)

        context['percentage_of_completed_projects'] = Step.objects.percentage_of_completed_projects(self.request.user)

        # Returns list of the tuples that contain SKU of project and percentage of completed project fields
        context['projects_progress'] = Project.objects.projects_progress(self.request.user)

        context["tickets"] = Ticket.objects.filter(raised_by = self.request.user)

        context["ticket_count"] = Ticket.objects.ticket_count(self.request.user)
        
        context["answered_ticket_count"] = Ticket.objects.answered_ticket_count(self.request.user)

        context["unanswered_ticket_count"] = Ticket.objects.unanswered_ticket_count(self.request.user)
        
        # List of applications that have not completed all steps
        context["inprogress_requests"] = Step.objects.inprogress_requests(self.request.user)

        # Number of applications that have not completed all steps +
        # if profile is not completed
        context["inprogress_requests_count"] = Step.objects.inprogress_requests_count(self.request.user)

        # List of applications that is completed all steps
        context["complete_requests"] = Step.objects.completed_requests(self.request.user)
        
        # Number of applications that is completed all steps +
        # if profile is completed
        context["complete_requests_count"] = Step.objects.completed_requests_count(self.request.user)

        context['whole_process'] = Step.objects.whole_process(self.request.user)

        return context