from django.urls import (
    path,
    re_path,
    include
)

from dashboard.views import HomeView

from accounts.views import (
    AccountProfileUpdateView,
    AccountProfileFinalizeView
)

from project.views import (
    ApplicationCreateView,
    ApplicationListView,
    ApplicationView,
    ApplicationFinalizeView,
    ProjectTemplateView,
    ProjectView,
    ProjectFinalizeView,
    ProjectShownView,
    TeamMemberView,
    TeamPublishedView
)


app_name = 'dashboard'

urlpatterns = [
    re_path(
        r'^$',
        HomeView.as_view(),
        name='home'
    ),
    re_path(
        r'^account/application/(?P<pk>\d+)/create/$',
        ApplicationCreateView.as_view(),
        name='application-create'
    ),
    re_path(
        r'^account/applications/$',
        ApplicationListView.as_view(),
        name='application-list'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/$',
        ApplicationView.as_view(),
        name='application-edit'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/published/$',
        ApplicationFinalizeView.as_view(),
        name='application-published'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/preview/$',
        ProjectTemplateView.as_view(),
        name='project-view'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/$',
        ProjectView.as_view(),
        name='application-project-edit'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/published/$',
        ProjectFinalizeView.as_view(),
        name='application-project-published'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/shown/$',
        ProjectShownView.as_view(),
        name='application-project-shown'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/team/members/$',
        TeamMemberView.as_view(),
        name = 'project-team-member'
    ),
    re_path(
        r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/team/published/$',
        TeamPublishedView.as_view(),
        name = 'project-team-published'
    ),
    re_path(
        r'^account/profile/(?P<sku_app>\w+\d*)/$',
        AccountProfileUpdateView.as_view(),
        name='account-profile-edit'
    ),
    re_path(
        r'^account/profile/(?P<sku_app>\w+\d*)/published/$',
        AccountProfileFinalizeView.as_view(),
        name='account-profile-published'
    ),
]