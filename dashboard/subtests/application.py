from django.test import TestCase

from django.contrib.auth import get_user_model

from django.urls import reverse

from project.models import Application


class ApplicationTestDashboardHomeView(TestCase):

    def setUp(self):

        User = get_user_model()

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

    
    def test_without_login(self):

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:home',
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )

    
    def test_context_related_to_applications(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertIn(
            'apps',
            response.context,
            msg = 'apps must be in response context.'
        )

        self.assertEqual(
            response.context.get('apps')[0],
            self.user.applications.all()[0],
            msg = 'User applications is incorrect.'
        )

        self.assertIn(
            'completed_applications_count',
            response.context,
            msg = 'completed_applications_count must be in response context.'
        )

        self.assertEqual(
            response.context.get('completed_applications_count'),
            0,
            msg = 'There is no completed application.'
        )

        self.assertIn(
            'percentage_of_completed_applications',
            response.context,
            msg = 'percentage_of_completed_applications must be in response context.'
        )

        self.assertEqual(
            response.context.get('percentage_of_completed_applications'),
            0,
            msg = 'There is no completed application.'
        )

        self.assertIn(
            'applications_progress',
            response.context,
            msg = 'applications_progress must be in response context.'
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                0.0
            ),
        )


    def test_application_progress(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        app = self.user.applications.all()[0]

        app.qatari_partnership = True

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                10.0
            ),
        )

        app.website = 'Sage'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                20.0
            ),
        )

        app.abstract = 'lorem ipsum'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                30.0
            ),
        )

        app.is_b2b = True

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                40.0
            ),
        )

        app.is_b2c = True

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                40.0
            ),
        )

        app.is_b2g = True

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                40.0
            ),
        )

        app.is_b2b2c = True

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                40.0
            ),
        )

        app.industry = 'lorem'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                50.0
            ),
        )

        app.sales_rate = '20000'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                60.0
            ),
        )

        app.sales_rate_type = 'm'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                70.0
            ),
        )

        app.deneb_result = 'lorem ipsum'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                80.0
            ),
        )

        app.keywords = 'lorem,ipsum,lorem'

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                90.0
            ),
        )

        app.is_published = True

        app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('applications_progress')[0],
            (
                self.user.applications.all()[0].sku,
                100.0
            ),
        )

    
    def test_percentage_of_completed_applications(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        first_app = self.user.applications.all()[0]

        first_app.is_published = True

        first_app.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('percentage_of_completed_applications'),
            100,
            msg = 'There is one application that is completed.'
        )

        Application.objects.create(user = self.user)

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('percentage_of_completed_applications'),
            50,
            msg = 'There are two application. there is one completed application.'
        )