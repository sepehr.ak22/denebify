import os

from django.test import TestCase

from django.conf import settings

from django.urls import reverse

from django.contrib.auth import get_user_model

from django.core.files.uploadedfile import SimpleUploadedFile


class ProfileTestDashboardHomeView(TestCase):

    def setUp(self):

        User = get_user_model()

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()


    def test_without_login(self):

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:home',
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )

    
    def test_context_related_to_profile(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertIn(
            'profile_progress',
            response.context,
            msg = 'profile_progress must be in response context.'
        )

        self.assertIn(
            'profile_status',
            response.context,
            msg = 'profile_status must be in response context.'
        )

        self.assertFalse(
            response.context.get('profile_status'),
            msg = 'profile is not completed.'
        )

        self.assertIn(
            'profile_progress_num',
            response.context,
            msg = 'profile_progress_num must be in response context.'
        )

        self.assertEqual(
            response.context.get('profile_progress_num'),
            11,
            msg = 'profile_progress_num must be 11.'
        )

    
    def test_profile_progress(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            0,
        )

        profile = self.user.profile

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()

                profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                profile.save()

                response = self.client.get(
                    reverse(
                        'dashboard:home',
                    ),
                )

                self.assertEqual(
                    response.context.get('profile_progress'),
                    1 / 11 * 100
                )

                profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)

                profile.save()

                response = self.client.get(
                    reverse(
                        'dashboard:home',
                    ),
                )

                self.assertEqual(
                    response.context.get('profile_progress'),
                    2 / 11 * 100
                )

        profile.email_confirmed = True
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            3 / 11 * 100
        )

        profile.phone_number = '+989111234567'
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            4 / 11 * 100
        )

        profile.is_published = True
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            5 / 11 * 100
        )

        profile.origin = 'US'
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            6 / 11 * 100
        )

        profile.residence = 'US'
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            7 / 11 * 100
        )

        profile.gender = 'm'
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            8 / 11 * 100
        )

        profile.bio = 'lorem ipsum'
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            9 / 11 * 100
        )

        profile.location = 'Kashmar, Iran',
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            10 / 11 * 100
        )

        profile.social_media = 'instagram.com/hmdbbgh'
        profile.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('profile_progress'),
            11 / 11 * 100
        )

        self.assertTrue(
            response.context.get('profile_status'),
            msg = 'profile is completed.'
        )