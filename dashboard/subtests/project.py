import os

from django.test import TestCase

from django.conf import settings

from django.urls import reverse

from django.contrib.auth import get_user_model

from django.core.files.uploadedfile import SimpleUploadedFile

from project.models import (
    Application,
    Team,
    Member
)


class ProjectTestDashboardHomeView(TestCase):

    def setUp(self):

        User = get_user_model()

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

    
    def __create_published_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr',
            'is_published': True
        }

        app = Application.objects.create(**data)
        return app


    def __create_published_project(self, app):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.problem = 'lorem ipsum'
                app.project.solution = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = True
                
                app.project.save()

    
    def __create_team(self, project):
        
        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': project,
            'is_published': False
        }

        team = Team.objects.create(**data)

        return team
    
    
    def __create_team_member(self, team):

        data = {
            'first_name': 'sepehr',
            'last_name': 'akbarzade',
            'position': 'CEO',
            'phone_number': '+0989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': team
        }

        team_member = Member.objects.create(**data)

    
    def __create_published_team(self, team):

        team.is_published = True

        team.save()

    
    def test_without_login(self):

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:home',
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in..",
            fetch_redirect_response = True
        )

    
    def test_context_related_to_projects(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertIn(
            'projects',
            response.context,
            msg = 'projects must be in response context.'
        )

        self.assertIn(
            'projects_count',
            response.context,
            msg = 'projects_count must be in response context.'
        )

        self.assertIn(
            'completed_projects_count',
            response.context,
            msg = 'completed_projects_count must be in response context.'
        )

        self.assertIn(
            'percentage_of_completed_projects',
            response.context,
            msg = 'percentage_of_completed_projects must be in response context.'
        )

        self.assertIn(
            'projects_progress',
            response.context,
            msg = 'projects_progress must be in response context.'
        )

    
    def test_projects_and_count(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertFalse(
            response.context.get('projects'),
            msg = 'There is no project.'
        )

        self.assertEqual(
            response.context.get('projects_count'),
            0,
            msg = 'projects_count must be 0.'
        )

        app = self.__create_published_application()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_count'),
            1,
            msg = 'There is a project.'
        )

        self.assertEqual(
            response.context.get('projects')[0],
            app.project,
            msg = 'Project is incorrect.'
        )

    
    def test_complete_project(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('completed_projects_count'),
            0,
            msg = 'There is no complete project.'
        )

        self.assertEqual(
            response.context.get('percentage_of_completed_projects'),
            0.0,
            msg = 'There is no complete project.'
        )

        app = self.__create_published_application()

        self.__create_published_project(app)

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_count'),
            1,
            msg = 'There is a project.'
        )

        self.assertEqual(
            response.context.get('completed_projects_count'),
            1,
            msg = 'There is a complete project.'
        )

        self.assertEqual(
            response.context.get('percentage_of_completed_projects'),
            100,
            msg = 'There is a complete project.'
        )


    def test_projects_progress(self):

        login_flag = self.client.login(
            username = 'hamed_test',
            password = 'h@med_1234'
        )

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress'),
            [],
            msg = 'There is a project.'
        )

        app = self.__create_published_application()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                0.0
            ),
            msg = 'There is a project.'
        )

        app.project.title = 'sage'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                1 / 15 * 100
            ),
        )

        app.project.summary = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                2 / 15 * 100
            ),
        )

        app.project.product = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                3 / 15 * 100
            ),
        )

        app.project.customer = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                4 / 15 * 100
            ),
        )

        app.project.business = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                5 / 15 * 100
            ),
        )

        app.project.market = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                6 / 15 * 100
            ),
        )

        app.project.competition = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                7 / 15 * 100
            ),
        )

        app.project.vision = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                8 / 15 * 100
            ),
        )

        app.project.founders = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                9 / 15 * 100
            ),
        )

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()

                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)

                app.project.save()

                response = self.client.get(
                    reverse(
                        'dashboard:home',
                    ),
                )

                self.assertEqual(
                    response.context.get('projects_progress')[0],
                    (
                        app.project.sku,
                        10 / 15 * 100
                    ),
                )

                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)

                app.project.save()

                response = self.client.get(
                    reverse(
                        'dashboard:home',
                    ),
                )

                self.assertEqual(
                    response.context.get('projects_progress')[0],
                    (
                        app.project.sku,
                        11 / 15 * 100
                    ),
                )
        

        app.project.problem = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                12 / 15 * 100
            ),
        )

        app.project.solution = 'lorem ipsum'

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                13 / 15 * 100
            ),
        )

        app.project.is_published = True

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                14 / 15 * 100
            ),
        )

        team = self.__create_team(app.project)

        self.__create_team_member(team)

        self.__create_published_team(team)

        app.project.is_shown = True

        app.project.save()

        response = self.client.get(
            reverse(
                'dashboard:home',
            ),
        )

        self.assertEqual(
            response.context.get('projects_progress')[0],
            (
                app.project.sku,
                15 / 15 * 100
            ),
        )