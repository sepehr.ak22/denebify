from django.dispatch import receiver

from django.db.models.signals import post_save

from django.core.mail import send_mail

from django.conf import settings

from ticket.models import (
    Department,
    Ticket,
    Comment
)


@receiver(post_save, sender=Ticket)
def massive_mailing_list(sender, instance, created, **kwargs):
    
    if created:
        
        department_users = [user for user in instance.department.users.all()]
        
        if instance.department.manager not in department_users:

            department_users.append(instance.department.manager)

        if department_users:

            send_email(department_users)
    

@receiver(post_save, sender=Comment)
def change_ticket_status(sender, instance, created, **kwargs):

    if created:

        department_users = [user for user in instance.ticket.department.users.all()]

        if instance.ticket.department.manager not in department_users:
            
            department_users.append(instance.ticket.department.manager)
        
        if instance.staff in department_users:

            ticket = instance.ticket
            ticket.answer_status = 'a'
            ticket.save()

            send_email([instance.ticket.raised_by])

        if instance.staff == instance.ticket.raised_by:

            ticket = instance.ticket
            ticket.answer_status = 'u'
            ticket.save()

            send_email(department_users)

        
def send_email(users):

    users_email = [user.email for user in users]

    send_mail(
            'Subject',
            'Here is the message.',
            settings.EMAIL_HOST_USER,
            users_email,
            fail_silently=False,
            )