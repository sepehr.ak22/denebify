from django.db import models

from django.conf import settings

from django.utils.translation import ugettext_lazy as _

from ticket.submodels.general import (
    Identification,
    TimestampedModel,
)

import secrets


class Comment(Identification, TimestampedModel):

    staff = models.ForeignKey(
                settings.AUTH_USER_MODEL, 
                related_name = 'comments', 
                on_delete = models.SET_NULL, 
                blank=True, 
                null=True
            )

    ticket = models.ForeignKey(
                'Ticket', 
                related_name='comments', 
                on_delete = models.SET_NULL, 
                blank=True, 
                null=True
            )

    message = models.TextField(max_length = 1000)


    class Meta:

        verbose_name = _('Comment')

        verbose_name_plural = _('Comments')

        ordering = ('-created',)


    def save(self, *args, **kwargs):

        if not self.pk:

            self.sku = secrets.token_hex(16)

        super(Comment, self).save(*args, **kwargs)


    def __str__(self):

        return f'A comment for <{self.ticket.subject}> ticket from <{self.staff.username}>'

        
    def __repr__(self):

        return f'A comment for <{self.ticket.subject}> ticket from <{self.staff.username}>'