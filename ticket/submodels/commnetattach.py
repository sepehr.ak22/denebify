from django.db import models

from django.utils.translation import ugettext_lazy as _

from ticket.submodels.general import (
    Identification,
    TimestampedModel
)

import secrets


class CommentAttach(Identification, TimestampedModel):

    comment = models.ForeignKey(
                'Comment',
                related_name = 'attachs',   
                on_delete = models.CASCADE  
            )

    uploaded_file = models.FileField(
                upload_to = 'upload/ticket',
                null = True,    
                blank = True    
            )


    def save(self, *args, **kwargs):

       if not self.pk:

            self.sku = secrets.token_hex(16)

       super(CommentAttach, self).save(*args, **kwargs)


    class Meta:

        verbose_name = _('Comment Attachment')

        verbose_name_plural = _('Comment Attachments')

        ordering = ('-created',)


    def __str__(self):

        return f'attach for comment of <{self.comment.ticket.subject}> ticket of <{self.comment.ticket.department.name}> department'

    
    def __repr__(self):

        return f'attach for comment of <{self.comment.ticket.subject}> ticket of <{self.comment.ticket.department.name}> department'