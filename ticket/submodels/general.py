from django.db import models

import secrets


class Identification(models.Model):

    sku = models.CharField(max_length = 256, editable = False, unique = True)


    class Meta:

        abstract = True


class TimestampedModel(models.Model):

    created = models.DateTimeField(auto_now_add = True)

    modified = models.DateTimeField(auto_now = True)
    

    class Meta:

        abstract = True


class AnswerStatus(models.Model):

    ANSWERED = 'a'
    UNANSWERED = 'u'

    STATUS = (
        (ANSWERED, 'Answered'),
        (UNANSWERED, 'Unanswered'),
    )

    answer_status = models.CharField(max_length = 1, choices = STATUS, default = UNANSWERED)

    message = models.TextField(max_length = 1000)


    class Meta:
        
        abstract = True


class PriorityStatus(models.Model):

    LOW = 'l'
    MEDIUM = 'm'
    HIGH = 'h'
    CRITICAL = 'c'

    STATUS = (
        (LOW, 'Low'),
        (MEDIUM, 'Medium'),
        (HIGH, 'High'),
        (CRITICAL, 'Critical'),
    )

    priority_status = models.CharField(max_length = 1, choices = STATUS, default = MEDIUM)


    class Meta:
        
        abstract = True