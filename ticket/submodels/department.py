from django.db import models

from django.conf import settings

from django.utils.translation import ugettext_lazy as _

from ticket.submodels.general import (
    Identification,
    TimestampedModel,
)

import secrets


class Department(Identification, TimestampedModel):

    name = models.CharField(max_length = 125)

    description = models.TextField()

    users = models.ManyToManyField(
                settings.AUTH_USER_MODEL, 
                related_name = "departments"
            )

    manager = models.OneToOneField(
                    settings.AUTH_USER_MODEL, 
                    related_name = "owner_department", 
                    on_delete = models.SET_NULL,
                    blank=True,
                    null=True
                ) 
    
    official = models.BooleanField(default = True)


    def save(self, *args, **kwargs):

       if not self.pk:

            self.sku = secrets.token_hex(16)

       super(Department, self).save(*args, **kwargs)


    class Meta:

        verbose_name = _('Department')

        verbose_name_plural = _('Departments')

        ordering = ('name',)


    def __str__(self):

        return f'<{self.name}> department'

    
    def __repr__(self):

        return f'<{self.name}> department'