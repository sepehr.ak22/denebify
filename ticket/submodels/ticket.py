import secrets

from django.db import models

from django.conf import settings

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from ticket.submodels.general import (
    TimestampedModel,
    Identification, 
    PriorityStatus, 
    AnswerStatus, 
)

from ticket.managers import TicketManager


class Ticket(Identification, TimestampedModel, AnswerStatus, PriorityStatus):

    department = models.ForeignKey(
                    'Department', 
                    related_name='tickets', 
                    on_delete = models.PROTECT, 
                )

    raised_by = models.ForeignKey(
                    settings.AUTH_USER_MODEL, 
                    related_name='tickets', 
                    on_delete=models.PROTECT, 
                )
    
    subject = models.CharField(max_length = 100)

    objects = TicketManager()


    def save(self, *args, **kwargs):

       if not self.pk:

            self.sku = secrets.token_hex(16)

       super(Ticket, self).save(*args, **kwargs)


    class Meta:

        verbose_name = _('Ticket')

        verbose_name_plural = _('Tickets')

        ordering = ('-created',)


    def __str__(self):

        return f'<{self.subject}> ticket of <{self.department.name}> department'

    
    def __repr__(self):

        return f'<{self.subject}> ticket of <{self.department.name}> department'