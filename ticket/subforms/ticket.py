import re

from django import forms

from django.utils.translation import ugettext_lazy as _

from ticket.models import Ticket


class TicketForm(forms.ModelForm):

    class Meta:

        model = Ticket

        fields = ('subject', 'priority_status', 'department', 'message', 'answer_status')

    
    def clean_subject(self):

        subject = self.cleaned_data['subject']

        subject_length = len(re.findall(r'\w+', subject))

        if not subject:
            
            raise forms.ValidationError(
                _('Please enter your ticket subject.'),
                code='forbidden'
            )
            
        if subject_length < 2:

            raise forms.ValidationError(
                _('Your ticket subject should be at least two words long.'),
                code='forbidden'
            )
        
        except_values = ['<', '>', '/']

        for e_v in except_values:

            if e_v in subject:

                raise forms.ValidationError(
                    _('You can not use \'<\' and \'>\' and \'/\' in your subject'),
                    code='forbidden'
                )

        return subject
    

    def clean_message(self):

        message = self.cleaned_data['message']

        if not message:

            raise forms.ValidationError(
                _('Please enter your ticket message.'),
                code='forbidden'
            )

        except_values = ['<', '>', '/']

        for e_v in except_values:

            if e_v in message:

                raise forms.ValidationError(
                    _('You can not use \'<\' and \'>\' and \'/\' in your message'),
                    code='forbidden'
                )

        return message