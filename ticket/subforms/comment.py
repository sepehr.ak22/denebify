from django import forms

from django.utils.translation import ugettext_lazy as _

from ticket.models import Comment


class CommentForm(forms.ModelForm):

    class Meta:

        model = Comment

        fields = ('message', 'staff', 'ticket',)


    def clean_message(self):

        message = self.cleaned_data['message']

        if not message:

            raise forms.ValidationError(
                _('Please enter your ticket message.'),
                code='forbidden'
            )

        except_values = ['<', '>', '/']

        for e_v in except_values:

            if e_v in message:

                raise forms.ValidationError(
                    _('You can not use \'<\' and \'>\' and \'/\' in your message'),
                    code='forbidden'
                )

        return message