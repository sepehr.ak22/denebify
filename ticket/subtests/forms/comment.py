from random import choice

from decouple import config

from functools import reduce

from django.core import mail

from django.test import TestCase

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from ticket.models import(
    Department,
    Ticket,
    Comment
)

from ticket.forms import CommentForm


class CommentFormTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
            email='hmdbbgh1011@gmail.com',
            password='h@med_1234'
        )

        self.ticket = self.__create_ticket()

    
    def __create_member(self):
         
        users = []

        for num in range(1, 6):

            user = User.objects.create(
                username='hamed{}_test'.format(num),
                email='hmdbbgh1011+{}@gmail.com'.format(num),
                password='h@med_1234'
            )

            users.append(user)
        
        return users


    def __create_department(self):

        users = self.__create_member()

        data = {
            'name': 'Finance',
            'description': """
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        """,
            'manager': users[-1],
            'official': True
        }

        department = Department.objects.create(**data)

        department.users.set(users[:-1])

        return department


    def __create_ticket(self):

        department = self.__create_department()

        data = {
            'department': department,
            'raised_by': self.user,
            'subject': "Lorem Ipsum",
            'answer_status': 'u',
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'priority_status': 'l'
        }

        ticket = Ticket.objects.create(**data)

        return ticket

    
    def __create_comment(self, staff):

        data = {
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """, 
            'staff': staff,
            'ticket': self.ticket
        }

        comment = Comment.objects.create(**data)

    
    def test_valid_comment_form(self):  

        data = {
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """, 
            'staff': self.user,
            'ticket': self.ticket
        } 

        form = CommentForm(data)

        self.assertTrue(
            form.is_valid(),
            msg = "Everything is fine but the comment form was denied."
        ) 


    def test_of_invalid_message(self):

        data = {
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and >typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """, 
            'staff': self.user,
            'ticket': self.ticket
        } 

        form = CommentForm(data)

        self.assertFalse(
            form.is_valid(),
            msg = "The message was invalid but the comment form accepted it."
        )


    def test_department_members_comment_and_ticket_answer_status(self):

        departmen_members = [member for member in self.ticket.department.users.all()]

        if self.ticket.department.manager not in self.ticket.department.users.all():

            departmen_members.append(self.ticket.department.manager)

        self.__create_comment(choice(departmen_members))

        self.assertEqual(
            self.ticket.answer_status,
            'a',
            msg = "Ticket answer status must be `answered`"
        )

    
    def test_ticket_user_comment_and_ticket_answer_status(self):

        departmen_members = [member for member in self.ticket.department.users.all()]

        if self.ticket.department.manager not in self.ticket.department.users.all():

            departmen_members.append(self.ticket.department.manager)

        self.__create_comment(self.user)

        self.assertEqual(
            self.ticket.answer_status,
            'u',
            msg = "Ticket answer status must be `unanswered`"
        )

    
    def test_department_members_comment_then_ticket_user_comment(self):

        departmen_members = [member for member in self.ticket.department.users.all()]

        if self.ticket.department.manager not in self.ticket.department.users.all():

            departmen_members.append(self.ticket.department.manager)

        self.assertEqual(
            self.ticket.answer_status,
            'u',
            msg = "At the first ticket answer status must be `unanswered`"
        )

        self.__create_comment(choice(departmen_members))

        self.assertEqual(
            self.ticket.answer_status,
            'a',
            msg = "Ticket answer status must be `answered`"
        )

        self.__create_comment(self.user)

        self.assertEqual(
            self.ticket.answer_status,
            'u',
            msg = "Ticket answer status must be `unanswered`"
        )

    
    def test_send_email_after_ticket_user_comment(self):

        departmen_members = [member for member in self.ticket.department.users.all()]

        if self.ticket.department.manager not in self.ticket.department.users.all():

            departmen_members.append(self.ticket.department.manager)

        self.__create_comment(self.user)
            
        self.assertEqual(
            len(mail.outbox),
            2,
            msg = "Two email must have been sent."
        )

        self.assertEqual(
            mail.outbox[1].subject,
            'Subject',
            msg = 'The subject of the email sent to the department is incorrect.'
        )

        self.assertEqual(
            mail.outbox[1].body,
            'Here is the message.',
            msg = 'The message of the email sent to the department is incorrect.'
        )

        self.assertEqual(
            mail.outbox[1].from_email,
            config('EMAIL_HOST_USER'),
            msg = 'The email sender is incorrect'
        )

        self.assertEqual(
            len(mail.outbox[1].to),
            len(departmen_members),
            msg = "The email should be sent to all members of the department and its manager."
        )

        departmen_members_email = [user.email for user in departmen_members]

        self.assertTrue(
            reduce(
                lambda i, j : i and j,
                map(
                    lambda m, k: m == k,
                    departmen_members_email,
                    mail.outbox[1].to
                ),
                True
            ),
            msg = "The people whom the email is sent are not exactly the members of our department."
        )

    
    def test_send_email_after_department_members_comment(self):

        departmen_members = [member for member in self.ticket.department.users.all()]

        if self.ticket.department.manager not in self.ticket.department.users.all():

            departmen_members.append(self.ticket.department.manager)

        self.__create_comment(choice(departmen_members))
            
        self.assertEqual(
            len(mail.outbox),
            2,
            msg = "Two email must have been sent."
        )

        self.assertEqual(
            mail.outbox[1].subject,
            'Subject',
            msg = 'The subject of the email sent to the department is incorrect.'
        )

        self.assertEqual(
            mail.outbox[1].body,
            'Here is the message.',
            msg = 'The message of the email sent to the department is incorrect.'
        )

        self.assertEqual(
            mail.outbox[1].from_email,
            config('EMAIL_HOST_USER'),
            msg = 'The email sender is incorrect'
        )

        self.assertEqual(
            len(mail.outbox[1].to),
            1,
            msg = "Email should only be sent to one person."
        )

        self.assertEqual(
            mail.outbox[1].to[0],
            self.ticket.raised_by.email,
            msg = "Email must be sent to ticket user."
        )