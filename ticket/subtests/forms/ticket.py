from decouple import config

from functools import reduce

from django.core import mail

from django.test import TestCase

from django.contrib.auth.models import User

from ticket.models import (
    Department,
    Ticket
)

from ticket.forms import TicketForm


class TicketFormTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
            email='hmdbbgh1011@gmail.com',
            password='h@med_1234'
        )

    
    def __create_member(self):
         
        users = []

        for num in range(1, 6):

            user = User.objects.create(
                username='hamed{}_test'.format(num),
                email='hmdbbgh1011+{}@gmail.com'.format(num),
                password='h@med_1234'
            )

            users.append(user)
        
        return users


    def __create_department(self):

        users = self.__create_member()

        data = {
            'name': 'Finance',
            'description': """
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        """,
            'manager': users[-1],
            'official': True
        }

        department = Department.objects.create(**data)

        department.users.set(users[:-1])

        return department


    def __create_ticket(self):

        department = self.__create_department()

        data = {
            'department': department,
            'raised_by': self.user,
            'subject': "Lorem Ipsum",
            'answer_status': 'u',
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'priority_status': 'l'
        }

        ticket = Ticket.objects.create(**data)

        return ticket

    
    def test_of_the_valid_ticket(self):

        department = self.__create_department()

        data = {
            'department': department,
            'raised_by': self.user,
            'subject': "Lorem Ipsum",
            'answer_status': 'u',
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'priority_status': 'l'
        }

        form = TicketForm(data)

        self.assertTrue(
            form.is_valid(),
            msg = "Everything is fine but the ticket form was denied."
        )
    
    
    def test_of_the_invalid_number_of_subject_words(self):

        department = self.__create_department()

        data = {
            'department': department,
            'raised_by': self.user,
            'subject': "Ipsum",
            'answer_status': 'u',
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'priority_status': 'l'
        }

        form = TicketForm(data)

        self.assertFalse(
            form.is_valid(),
            msg = "The subject was invalid but the ticket form accepted it."
        )
        

    def test_of_invalid_subject(self):

        department = self.__create_department()

        data = {
            'department': department,
            'raised_by': self.user,
            'subject': "Lorem Ip<sum",
            'answer_status': 'u',
            'message':"""
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'priority_status': 'l'
        }

        form = TicketForm(data)

        self.assertFalse(
            form.is_valid(),
            msg = "The subject was invalid but the ticket form accepted it."
        )
    
    
    def test_of_invalid_message(self):

        department = self.__create_department()

        data = {
            'department': department,
            'raised_by': self.user,
            'subject': "Lorem Ipsum",
            'answer_status': 'u',
            'message':"""
                        Lorem Ipsum is simply dummy >text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'priority_status': 'l'
        }

        form = TicketForm(data)

        self.assertFalse(
            form.is_valid(),
            msg = "The message was invalid but the ticket form accepted it."
        )

    
    def test_send_email(self):

        ticket = self.__create_ticket()

        departmen_members = [member for member in ticket.department.users.all()]

        if ticket.department.manager not in ticket.department.users.all():

            departmen_members.append(ticket.department.manager)
            
        self.assertEqual(
            len(mail.outbox),
            1,
            msg = "An email must have been sent."
        )

        self.assertEqual(
            mail.outbox[0].subject,
            'Subject',
            msg = 'The subject of the email sent to the department is incorrect.'
        )

        self.assertEqual(
            mail.outbox[0].body,
            'Here is the message.',
            msg = 'The message of the email sent to the department is incorrect.'
        )

        self.assertEqual(
            mail.outbox[0].from_email,
            config('EMAIL_HOST_USER'),
            msg = 'The email sender is incorrect'
        )

        self.assertEqual(
            len(mail.outbox[0].to),
            len(departmen_members),
            msg = "The email should be sent to all members of the department and its manager."
        )

        departmen_members_email = [user.email for user in departmen_members]

        self.assertTrue(
            reduce(
                lambda i, j : i and j,
                map(
                    lambda m, k: m == k,
                    departmen_members_email,
                    mail.outbox[0].to
                ),
                True
            ),
            msg = "The people whom the email is sent are not exactly the members of our department."
        )