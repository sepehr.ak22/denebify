from django.db import models


class TicketQuerySet(models.QuerySet):

    def ticket_count(self, user):

        return self.filter(raised_by = user)

    
    def answered_ticket_count(self, user):

        return self.filter(
            raised_by = user,
            answer_status = 'a'
        )

    
    def unanswered_ticket_count(self, user):

        return self.filter(
            raised_by = user,
            answer_status = 'u'
        )


class TicketManager(models.Manager):

    def get_queryset(self):

        return TicketQuerySet(self.model, using = self._db)  # Important!


    def ticket_count(self, user):

        return self.get_queryset().ticket_count(user).count()

    
    def answered_ticket_count(self, user):

        return self.get_queryset().answered_ticket_count(user).count()

    
    def unanswered_ticket_count(self, user):

        return self.get_queryset().unanswered_ticket_count(user).count()