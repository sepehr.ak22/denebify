from ticket.submodels.department import Department
from ticket.submodels.ticket import Ticket
from ticket.submodels.comment import Comment
from ticket.submodels.ticketattach import TicketAttach
from ticket.submodels.commnetattach import CommentAttach