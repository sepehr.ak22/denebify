from django.contrib import admin

from  ticket.models import CommentAttach

from ticket.subadmins.general import GeneralAdminDashboardDesign


@admin.register(CommentAttach)
class CommentAttachAdmin(GeneralAdminDashboardDesign):

    search_fields = (
        'sku',
        'comment__ticket__subject',
        'comment__ticket__department__name'
    )
    
    list_filter = (
        'created',
        'comment__ticket__department',
        'comment__ticket',
        'comment',
    )

    list_display = ('comment',)

    list_display_links = ('comment',)

    fields = (
        ('comment',),
        ('uploaded_file',),
    )