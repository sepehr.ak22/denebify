from django.contrib import admin

from  ticket.models import TicketAttach

from ticket.subadmins.general import GeneralAdminDashboardDesign


@admin.register(TicketAttach)
class TicketAttachAdmin(GeneralAdminDashboardDesign):

    search_fields = (
        'sku',
        'ticket__subject',
        'ticket__department__name'
    )
    
    list_filter = (
        'created',
        'ticket__department',
        'ticket',
    )

    list_display = ('ticket',)

    list_display_links = ('ticket',)

    fields = (
        ('ticket',),
        ('uploaded_file',),
    )