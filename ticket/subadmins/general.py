from django.contrib import admin


class GeneralAdminDashboardDesign(admin.ModelAdmin):
     
    date_hierarchy = 'created'
    
    actions_on_top = True 
    
    actions_on_bottom = True
    
    actions_selection_counter = True
    
    empty_value_display = True