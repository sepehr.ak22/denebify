def make_Answered(modeladmin, request, queryset):

     rows_updated = queryset.update(answer_status = 'a')

     message_bit = "1 ticket was" if rows_updated == 1 else "{} tickets were".format(rows_updated)

     modeladmin.message_user(request, "{} successfully marked as answered.".format(message_bit))
make_Answered.short_description = "Marked selected tickets to answered"


def make_Unanswered(modeladmin, request, queryset):
     rows_updated = queryset.update(answer_status = 'u')

     message_bit = "1 ticket was" if rows_updated == 1 else "{} tickets were".format(rows_updated)	

     modeladmin.message_user(request, "{} successfully marked as unanswered.".format(message_bit))
make_Unanswered.short_description = "Marked selected tickets to unanswered"