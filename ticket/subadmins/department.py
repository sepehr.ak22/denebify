from django.contrib import admin

from ticket.models import Department

from ticket.subadmins.general import GeneralAdminDashboardDesign


@admin.register(Department)
class DepartmentAdmin(GeneralAdminDashboardDesign):

    search_fields = (
        'name',
        'manager__username'
    )

    list_filter = (
        'created',
        'official'
    )

    filter_horizontal = ('users',)
    
    list_display = (
        'name',
        'manager',
        'official'
    )

    fieldsets = (
        (None, {
            'fields': (
                ('name', ),
                ('description', ),
            ),
        }),
        ('Advanced Options', {
            'classes': ('collapse', ),
            'fields': (
                ('manager', 'official'),
                ('users')
            ),
        }),
    )