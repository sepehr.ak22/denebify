from django.contrib import admin

from  ticket.models import Comment

from ticket.subadmins.general import GeneralAdminDashboardDesign

from ticket.subadmins.actions.ticket import (
    make_Answered, 
    make_Unanswered
)


@admin.register(Comment)
class CommentAdmin(GeneralAdminDashboardDesign):

    search_fields = (
        'sku', 
        'staff__username',
        'ticket__subject', 
        'ticket__department__name', 
    )

    list_filter = (
        'created', 
        'ticket__department',
        'ticket__answer_status',
    )

    list_display = (
        'ticket', 
        'staff', 
        'is_answered'
    )

    list_display_links = ('ticket',)

    actions = (make_Answered, make_Unanswered)

    fieldsets = (
        (None, {
            'fields': (
                ('message',),
                ('ticket',),
                ('staff',),
            ),
        }),
        ('Comment Status', {
            'fields': (
                ('answer_status',),
            ),
        }),
    )
    
    
    def is_answered(self, obj):
        return True if obj.answer_status == 'a' else False
    is_answered.boolean = True
    is_answered.short_description = 'Is Answered'
    is_answered.admin_order_field = 'answer_status'


