from django.contrib import admin

from ticket.models import Ticket

from ticket.subadmins.general import GeneralAdminDashboardDesign

from ticket.subadmins.actions.ticket import (
    make_Answered, 
    make_Unanswered
)


@admin.register(Ticket)
class TicketAdmin(GeneralAdminDashboardDesign):

    search_fields = (
        'sku',
        'subject',
        'department__name'
    )

    list_filter = (
        'created',
        'department', 
        'answer_status',
        'priority_status',
    )

    list_display = (
        'get_department_name', 
        'subject', 
        'raised_by', 
        'priority_status', 
        'is_answered', 
    )

    actions = (make_Answered, make_Unanswered)

    fieldsets = (
        (None, {
            'fields': (
                ('subject'),
                ('message',),
                ('department',),
                ('raised_by',),
            ),
        }),
        ('Ticket Status', {
            'fields': (
                ('priority_status', ),
                ('answer_status', ),
            ),
        }),
    )


    def is_answered(self, obj):
        return True if obj.answer_status == 'a' else False
    is_answered.boolean = True


    def get_department_name(self, obj):
        return obj.department.name
    get_department_name.short_description = 'Department'
    get_department_name.admin_order_field = 'department__name'