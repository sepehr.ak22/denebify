from ticket.subadmins.department import DepartmentAdmin
from ticket.subadmins.ticket import TicketAdmin
from ticket.subadmins.comment import CommentAdmin
from ticket.subadmins.ticketattach import TicketAttachAdmin
from ticket.subadmins.commentattach import CommentAttachAdmin