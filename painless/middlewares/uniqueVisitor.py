from django.conf import settings 
from accounts.models import UniqueUserVisit
from django.utils.deprecation import MiddlewareMixin
from importlib import import_module

class TrackUniqueVisitsMiddleware(MiddlewareMixin):
    def __init__(self, get_response=None):
        self.get_response = get_response
        engine = import_module(settings.SESSION_ENGINE)
        self.SessionStore = engine.SessionStore

    def process_request(self, request):
        if not request.user.is_authenticated:
            return

        if request.resolver_match.namespace in settings.NAMESPACES_TO_TRACK:
            UniqueUserVisit.objects.get_or_create(
                user=request.user,
                namespace=request.resolver_match.namespace,
                view_name=request.resolver_match.view_name
            )