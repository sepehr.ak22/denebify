from django.core.exceptions import ValidationError
from moviepy.editor import VideoFileClip

from django.conf import settings

def FileSize(value):
    """max_upload_size - a number indicating the maximum file size allowed for upload.
        2.5MB - 2621440
        5MB - 5242880
        10MB - 10485760
        20MB - 20971520
        50MB - 5242880
        100MB 104857600
        250MB - 214958080
        500MB - 429916160
    """

    if value.size > settings.MAX_UPLOAD_SIZE:
        raise ValidationError('File too large. Size should not exceed Bytes.'.format(settings.MAX_UPLOAD_SIZE))

def ExtensionValidator(value):
    if not value.name.endswith('.mp4'):
        raise ValidationError('File Extension is invalid. You must enter a mp4 file.')