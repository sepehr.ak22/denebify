from rest_framework.permissions import (
    BasePermission, 
    DjangoModelPermissions
)

from rest_framework import exceptions


class AppViewPermission(BasePermission):
    
    perms_map = {
     'GET': ['%(app_label)s.view_%(model_name)s']
    }

    
    def get_required_permissions(self, method, model_cls):
        
        kwargs = {
            'app_label': model_cls._meta.app_label,
            'model_name': model_cls._meta.model_name
        }

        if method not in self.perms_map:
            raise exceptions.MethodNotAllowed(method)

        return [perm % kwargs for perm in self.perms_map[method]]
    
    
    def has_permission(self, request, model_cls):

        perm = self.get_required_permissions('GET', model_cls)

        return bool(request.user and request.user.has_perms(perm))