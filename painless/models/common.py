from django.db import models

from django.utils.text import slugify

from django.utils.translation import ugettext_lazy as _

from django.core.validators import FileExtensionValidator

from painless.upload_features.upload_path import images_upload_to

from markdown_deux.templatetags.markdown_deux_tags import markdown_allowed

from ckeditor.fields import RichTextField
from ckeditor.fields import RichTextUploadingField

class TimeStamp(models.Model):
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        proxy = True

class BasicContent(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    summary = models.TextField(_("Summary"), max_length=512)
    content = models.TextField(_("Content"))

    class Meta:
        proxy = True
    
    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(BasicContent, self).save(*args, **kwargs)

class CKEditorContent(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    summary = RichTextField(_("Summary"), max_length=512)
    content = RichTextField(_("Content"))

    class Meta:
        proxy = True
    
    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(CKEditorContent, self).save(*args, **kwargs)

class CKEditorUploadContent(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    summary = RichTextUploadingField(_("Summary"), max_length=512)
    content = RichTextUploadingField(_("Content"))

    class Meta:
        proxy = True
    
    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(CKEditorUploadContent(models, self).save(*args, **kwargs))

class MarkDownContent(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    summary = models.TextField(_("Summary"), max_length=512)
    content = models.TextField(_("Content"), help_text=markdown_allowed())

    class Meta:
        proxy = True
    
    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(CKEditorUploadContent(models, self).save(*args, **kwargs))

class BasicPicture(models.Model):
    
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Picture"), 
                upload_to=images_upload_to, 
                height_field='height_field', 
                width_field='width_field', 
                validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png', 'jpeg', 'JPG', 'JPEG', 'PNG'])],
                max_length=100)

    alternative_pic = models.CharField(_("Alternative Text"), max_length=50)

    class Meta:
        proxy = True

class UnRequiredPicture(models.Model):
    
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Picture"), 
                upload_to=images_upload_to, 
                height_field='height_field', 
                width_field='width_field', 
                validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png'])],
                max_length=100,
                blank=True,
                null=True)

    alternative_pic = models.CharField(_("Alternative Text"), max_length=50)

    class Meta:
        proxy = True

class ScalableVectorGraphic(models.Model):
    svg = models.FileField(
        upload_to=images_upload_to, 
        max_length=100, 
        validators=[FileExtensionValidator(['svg', 'png', 'SVG', 'PNG'])])
    
    alternative = models.CharField(_("Alternative Text"), max_length=50)

    class Meta:
        proxy = True

class UnRequiredScalableVectorGraphic(models.Model):
    svg = models.FileField(
        upload_to=images_upload_to, 
        max_length=100, 
        validators=[FileExtensionValidator(['svg', 'png', 'SVG', 'PNG'])],
        blank=True,
        null=True)
    
    alternative = models.CharField(_("Alternative Text"), max_length=50)

    class Meta:
        proxy = True

class ThumbnailPicture(models.Model):
    # fields

    class Meta:
        proxy = True