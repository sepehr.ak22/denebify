from datetime import date

def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.email, filename)

def user_directory_path_project(instance, filename):
    return 'user_{0}/{1}'.format(instance.application.user.email, filename)

def date_directory_path(instance, filename):
    today = date.today()
    return f'{today.year}/{today.month}/{today.day}/{filename}'
