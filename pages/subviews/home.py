from django.shortcuts import render

from django.views.generic import CreateView

from django.urls import reverse_lazy

from django.utils.translation import ugettext_lazy as _

from django.contrib.messages.views import SuccessMessageMixin

from pages.models import (
    Site,
    Contact
)

from project.models import (
    Project,
    Application
)

from blog.models import Post


class HomeView(SuccessMessageMixin, CreateView):

    template_name = "pages/index.html"

    page_name = 'Home'

    model = Contact

    fields = ('first_name', 'last_name', 'email', 'phone', 'project', 'country', 'message',)

    success_url = reverse_lazy('pages:contact')

    success_message = _("Your message has been saved. We will contact you soon.")

    def get_context_data(self, **kwargs):
        
        context = super(HomeView, self).get_context_data(**kwargs)

        context['site'] = Site.objects.first()

        apps = Application.objects.all()

        context['projects'] = Project.objects.filter(application__in = apps, is_shown = True, is_published = True)

        context['recent_posts'] = Post.objects.all().order_by('-created')[:3]

        return context