from django.views.generic import (
    TemplateView,
    CreateView,
    DetailView
)

from django.urls import reverse_lazy

from django.utils.translation import ugettext_lazy as _

from django.contrib.messages.views import SuccessMessageMixin

from pages.models import (
    Site,
    Page,
    Contact
)


class ContactView(SuccessMessageMixin, CreateView):

    template_name = "pages/contact.html"

    page_name = 'Contact'

    model = Contact

    fields = ('first_name', 'last_name', 'email', 'phone', 'project', 'country', 'message',)

    success_url = reverse_lazy('pages:contact')

    success_message = _("Your message has been saved. We will contact you soon.")


    def get_context_data(self, **kwargs):

        context = super(ContactView, self).get_context_data(**kwargs)

        context['site'] = Site.objects.first()

        context['page'] = Page.objects.get(title = 'Contact')
        
        return context

    
    def recaptcha_validation(self, form):

        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': form.data['g-recaptcha-response'],
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False

    
    def form_valid(self, form):
        
        form.save(commit=False)
        
        # if self.recaptcha_validation(form): 
        form.save()
        return super(ContactView, self).form_valid(form)
        # else:
        #     return super(ContactView, self).form_invalid(form)