from django.views.generic import TemplateView

from pages.models import (
    Site,
    Page
)


class AboutView(TemplateView):

    template_name = "pages/about.html"

    page_name = 'About'

    def get_context_data(self, **kwargs):
        
        context = super(AboutView, self).get_context_data(**kwargs)

        context['site'] = Site.objects.first()

        context['page'] = Page.objects.get(title = 'About')
        
        return context