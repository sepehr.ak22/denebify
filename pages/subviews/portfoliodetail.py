from hitcount.views import HitCountDetailView

from pages.models import (
    Site,
    Page
)

from project.models import Project


class PortfolioDetailView(HitCountDetailView):

    template_name = "pages/portfolio-detail.html"

    page_name = 'Portfolio'

    model = Project

    slug_field = 'slug'

    context_object_name  = 'project'

    count_hit = True

    def get_context_data(self, **kwargs):

        context = super(PortfolioDetailView, self).get_context_data(**kwargs)

        context['site'] = Site.objects.first()

        context['recent_projects'] = Project.objects.filter(is_shown = True, is_published = True).order_by('-created')[:5]

        context['page'] = Page.objects.get(title = 'Portfolio')

        return context