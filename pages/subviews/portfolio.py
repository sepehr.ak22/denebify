from django.views.generic import TemplateView

from pages.models import (
    Site,
    Page
)

from project.models import (
    Application,
    Project
)


class PortfolioView(TemplateView):

    template_name = "pages/portfolio.html"

    page_name = 'Portfolio'


    def get_context_data(self, **kwargs):

        context = super(PortfolioView, self).get_context_data(**kwargs)

        context['site'] = Site.objects.first()

        context['page'] = Page.objects.get(title = 'Portfolio')

        apps = Application.objects.all()

        context['projects'] = Project.objects.filter(application__in = apps, is_shown = True, is_published = True)

        return context