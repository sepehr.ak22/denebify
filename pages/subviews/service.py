from django.views.generic import TemplateView

from pages.models import (
    Site,
    Page
)


class ServiceView(TemplateView):

    template_name = "pages/service.html"

    page_name = 'Services'


    def get_context_data(self, **kwargs):

        context = super(ServiceView, self).get_context_data(**kwargs)

        context['site'] = Site.objects.first()

        context['page'] = Page.objects.get(title = 'Service')
        
        return context