from pages.subviews.home import HomeView
from pages.subviews.service import ServiceView
from pages.subviews.about import AboutView
from pages.subviews.contact import ContactView
from pages.subviews.portfolio import PortfolioView
from pages.subviews.portfoliodetail import PortfolioDetailView