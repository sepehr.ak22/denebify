import re

from django.utils.translation import ugettext_lazy as _

from django import forms
from .models import Contact
from painless.regex.patterns import PERSIAN_PHONE_NUMBER_PATTERN

from django_countries.widgets import CountrySelectWidget

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'email', 'phone', 'project', 'country', 'message',]

        widgets = {
            'country': CountrySelectWidget(),
        }

    def clean_phone(self):
        data = self.cleaned_data['phone']
        if data:
            if not re.match(PERSIAN_PHONE_NUMBER_PATTERN, data):
                raise forms.ValidationError(_('Invalid value: %(value)s'), code='invalid', params={'value': data})
        return data