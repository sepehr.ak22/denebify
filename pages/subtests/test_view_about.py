import os

from django.test import (
    TestCase,
    Client,
    RequestFactory
)

from django.conf import settings

from django.contrib.auth.models import User

from django.core.files.uploadedfile import SimpleUploadedFile

from pages.views import AboutView

from pages.models import (
    Site,
    Page
)


class AboutViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
            email='hmdbbgh1011@gmail.com',
            password='h@med_1234'
        )

        self.pages = [self.__create_page('About')]

        # self.client = Client(enforce_csrf_checks = True)

        self.request = RequestFactory().get('/about/')


    def __create_site(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name

            uploaded_picture_file = uploaded_picture.read()

            data = {
                'name': 'sage',
                'address': 'iran/tehran',
                'domain': 'sage.com',
                'phone_number': '+989011231213',
                'footer': """
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        """,
                'about_pic': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                'about_us': """
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        """,
                'alternative_about_pic': 'Lorem Ipsum',
                'logo': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                'achievement_logo': 'Lorem Ipsum',
                'insta': 'www.instagram.com',
                'facebook': 'www.facebook.com',
                'mail': 'sage@gmail.com',
            }

            site = Site.objects.create(**data)

            return site
    
    
    def __create_page(self, title):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name

            uploaded_picture_file = uploaded_picture.read()

            data = {
                'title': title, 
                'banner': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
            }

            page = Page.objects.create(**data)

            return page
    
    
    def test_empty_get(self):

        response = self.client.get('/about/')

        self.assertEqual(response.status_code, 200)

    
    def test_site_in_context(self):

        response = self.client.get('/about/')

        self.assertIn('site', response.context)


    def test_site_instance_in_context(self):
        
        sites = list()

        sites.append(self.__create_site())

        response = self.client.get('/about/')

        self.assertIn('site', response.context)

        self.assertTrue(
            response.context['site'],
            msg = 'There must be a site.'
        )

        sites.append(self.__create_site())
        
        response = self.client.get('/')
        
        self.assertEqual(
            response.context['site'],
            sites[0],
            msg = 'There must be same.'
        )

    
    def test_page_in_context(self):

        response = self.client.get('/about/')

        self.assertIn('page', response.context)

    
    def test_page_instance_in_context(self):

        response = self.client.get('/about/')

        self.assertIn('page', response.context)

        self.assertTrue(
            response.context['page'],
            msg = 'There must be a site.'
        )

        self.pages.append(self.__create_page('Service'))
        
        response = self.client.get('/about/')
        
        self.assertEqual(
            response.context['page'],
            self.pages[0],
            msg = 'There must be same.'
        )