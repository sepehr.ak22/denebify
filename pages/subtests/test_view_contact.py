import os

from decouple import config

from functools import reduce

from django.test import (
    TestCase,
    Client,
    RequestFactory
)

from django.urls import reverse

from django.conf import settings

from django.contrib.auth.models import User

from django.views.decorators.csrf import ensure_csrf_cookie

from django.core.files.uploadedfile import SimpleUploadedFile

from pages.views import ContactView

from pages.models import (
    Site,
    Page,
    Contact
)

from project.models import (
    Project,
    Application,
    Team,
    Member
)

from blog.models import (
    Post,
    Category
)


class ContactViewTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
            email='hmdbbgh1011@gmail.com',
            password='h@med_1234'
        )

        self.pages = [self.__create_page('Contact')]

        self.client = Client(enforce_csrf_checks = True)


    def __create_site(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name

            uploaded_picture_file = uploaded_picture.read()

            data = {
                'name': 'sage',
                'address': 'iran/tehran',
                'domain': 'sage.com',
                'phone_number': '+989011231213',
                'footer': """
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        """,
                'about_pic': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                'about_us': """
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        """,
                'alternative_about_pic': 'Lorem Ipsum',
                'logo': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                'achievement_logo': 'Lorem Ipsum',
                'insta': 'www.instagram.com',
                'facebook': 'www.facebook.com',
                'mail': 'sage@gmail.com',
            }

            site = Site.objects.create(**data)

            return site
    
    
    def __create_page(self, title):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name

            uploaded_picture_file = uploaded_picture.read()

            data = {
                'title': title, 
                'banner': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
            }

            page = Page.objects.create(**data)

            return page
    
    
    def __create_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': True,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr',
            'is_published': True
        }

        app = Application.objects.create(**data)
        return app

    
    def __create_draft_project(self):

        app = self.__create_application()

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = False
                
                app.project.save()

                self.project = app.project

  
    def __create_published_project(self):

        self.__create_team()
        
        self.__create_team_member()

        self.__create_published_team()
        
        self.project.is_published = True
        self.project.save()

    
    def __create_team(self):
        
        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': self.project,
            'is_published': False
        }

        self.team = Team.objects.create(**data)
    
    
    def __create_team_member(self):

        data = {
            'first_name': 'sepehr',
            'last_name': 'akbarzade',
            'position': 'CEO',
            'phone_number': '+0989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }

        self.team_member = Member.objects.create(**data)

    
    def __create_published_team(self):

        self.team.is_published = True

        self.team.save()

    
    def __create_is_shown_project(self):

        self.project.is_shown = True
        
        self.project.save()
    
    
    def __get_token(self, url, data):
        
        resp = self.client.get(url)
        
        data['csrfmiddlewaretoken'] = resp.cookies['csrftoken'].value
        
        return data
    

    def test_site_in_context(self):

        response = self.client.get('/contact/')

        self.assertIn('site', response.context)


    def test_site_instance_in_context(self):
        
        sites = list()

        sites.append(self.__create_site())

        response = self.client.get('/contact/')

        self.assertIn('site', response.context)

        self.assertTrue(
            response.context['site'],
            msg = 'There must be a site.'
        )

        sites.append(self.__create_site())
        
        response = self.client.get('/contact/')
        
        self.assertEqual(
            response.context['site'],
            sites[0],
            msg = 'There must be same.'
        )

    
    def test_page_in_context(self):

        response = self.client.get('/contact/')

        self.assertIn('page', response.context)

    
    def test_page_instance_in_context(self):

        response = self.client.get('/contact/')

        self.assertIn('page', response.context)

        self.assertTrue(
            response.context['page'],
            msg = 'There must be a site.'
        )

        self.pages.append(self.__create_page('About Us'))
        
        response = self.client.get('/contact/')
        
        self.assertEqual(
            response.context['page'],
            self.pages[0],
            msg = 'There must be same.'
        )

    
    def test_empty_get(self):

        response = self.client.get('/contact/')

        self.assertEqual(response.status_code, 200)


    def test_post_contact(self):

        self.__create_draft_project()

        self.__create_published_project()

        self.__create_is_shown_project()

        data = {
            'first_name': 'user_test',
            'last_name': 'user_test',
            'email': 'user_test@gmail.com',
            'phone': '090112345678',
            'project': 'new project',
            'country': 'US',
            'message': """
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    """,
            'g-recaptcha-response': config('RECAPTCHA_SITE_KEY')
        }

        data = self.__get_token('/contact/', data)

        response = self.client.post('/contact/', data=data, follow=True)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            Contact.objects.count(),
            1,
            msg = 'There must be a contact'
        )

        self.assertIn(b'Your message has been saved. We will contact you soon.', response.content)