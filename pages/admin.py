from django.contrib import admin

from .models import Site
from .models import Banner
from .models import Service
from .models import WorkFlow
from .models import PortfolioCategory
from .models import Portfolio
from .models import Pricing
from .models import PriceFeature
from .models import Testimonial
from .models import Page
from .models import Contact
from .models import WorkExperience

admin.site.register(Site)
admin.site.register(WorkFlow)
admin.site.register(PortfolioCategory)
admin.site.register(Portfolio)
admin.site.register(Pricing)
admin.site.register(Testimonial)
admin.site.register(Page)
admin.site.register(WorkExperience)

@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    '''Admin View for Service'''

    list_display = ('slug', 'is_main', 'width_field', 'height_field', 'priority')
    list_filter = ('is_main',)
    search_fields = ('title',)
    list_editable = ('is_main', 'priority')

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone', 'project', 'message',)
    list_filter = ('created',)
    search_fields = ('first_name', 'last_name', 'email', 'country',)
    ordering = ('-created',)

@admin.register(PriceFeature)
class PriceFeatureAdmin(admin.ModelAdmin):
    list_display = ('title', 'disabled', 'pricing',)
    list_filter = ('disabled', 'pricing')
    search_fields = ('title',)

@admin.register(Banner)
class PriceFeatureAdmin(admin.ModelAdmin):
    list_display = ('title_black', 'title_gold', 'subtitle',)
    search_fields = ('title_black',)