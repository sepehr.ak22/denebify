from django.db import models
from django.core.validators import FileExtensionValidator
from django.core.validators import MaxLengthValidator
from django.core.validators import MinLengthValidator
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
from django.core.validators import EmailValidator

from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django_countries.fields import CountryField
from ckeditor.fields import RichTextField

class Site(models.Model):
    name = models.CharField(_("Website Name"), max_length=200)
    
    
    address = models.CharField(_("Address"), max_length=200)
    domain = models.URLField(_("Domain"), max_length=200)
    phone_number = models.CharField(_("Phone Number"), max_length = 15, help_text = "+98912...")
    footer = RichTextField(_("Footer"))
    
    # about section
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    about_pic = models.ImageField(_("About Pic"), upload_to='site', height_field='height_field', width_field='width_field', max_length=100)
    about_us = RichTextField(_("About us"))
    alternative_about_pic = models.CharField(_("Alternative Text"), max_length=50)

    logo = models.FileField(upload_to='site/',  max_length=100, validators=[FileExtensionValidator(['svg', 'png'])])
    achievement_logo = models.CharField(_("Alternative Logo"), max_length=50)

    insta = models.URLField(_("Instagram"), max_length=200)
    facebook = models.URLField(_("Facebook"), max_length=200)
    mail = models.EmailField(_("Mail"), max_length=200)

    class Meta:
        verbose_name = _('Site')
        verbose_name_plural = _('Sites')

    def __str__(self):
        return self.domain

    def save(self, *args, **kwargs):
       super(Site, self).save(*args, **kwargs)

class Banner(models.Model):

    title_black = models.CharField(_("Title black"), max_length=60)
    title_gold = models.CharField(_("Title gold"), max_length=60)
    subtitle = models.CharField(_("Subtitle"), max_length=254)
    summary = RichTextField(_("Summary"), max_length=512)

    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Picture"), upload_to='site/banner', height_field='height_field', width_field='width_field', max_length=100)

    alternative_pic = models.CharField(_("Alternative Text"), max_length=50)
    
    class Meta:
        """Meta definition for Banner."""

        verbose_name = _('Banner')
        verbose_name_plural = _('Banners')

    def __str__(self):
        return self.title_black

    def save(self, *args, **kwargs):
       super(Banner, self).save(*args, **kwargs) # Call the real save() method

class Service(models.Model):

    title = models.CharField(_("Title black"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    summary = RichTextField(_("Summary"), max_length=512)
    content = RichTextField(_("Content"))
    is_main = models.BooleanField(_("Is main"), default = False)
    
    alternative_icon = models.CharField(_("Alternative Text"), max_length=50)
    icon_svg = models.FileField(upload_to='site/',  max_length=100, validators=[FileExtensionValidator(['svg', 'png'])])
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Picture"), upload_to='site/Portfolio', height_field='height_field', width_field='width_field', max_length=100, validators=[FileExtensionValidator(['jpg', 'png'])])
    alternative_pic = models.CharField(_("Alternative Pic"), max_length=50)

    priority = models.PositiveSmallIntegerField(_("Priority"))

    site = models.ForeignKey('Site', related_name='services', on_delete=models.CASCADE)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    
    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')
        ordering = ("priority",)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Service, self).save(*args, **kwargs)

class WorkFlow(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    summary = RichTextField(_("Summary"))
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Workflow"), upload_to='site', height_field='height_field', width_field='width_field', max_length=100, validators=[FileExtensionValidator(['jpg', 'png'])])

    site = models.ForeignKey('Site', related_name='flows', on_delete=models.CASCADE)

    alternative_pic = models.CharField(_("Alternative Pic"), max_length=50)
    alternative_svg = models.CharField(_("Alternative SVG"), max_length=50)
    icon_svg = models.FileField(upload_to='site/',  max_length=100, validators=[FileExtensionValidator(['svg', 'png'])])

    class Meta:
        """Meta definition for WorkFlow."""

        verbose_name = _('WorkFlow')
        verbose_name_plural = _('WorkFlows')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(WorkFlow, self).save(*args, **kwargs)

class WorkExperience(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    subtitle = models.CharField(_("Subtitle"), max_length=60)
    summary = RichTextField(_("Summary"))
    priority = models.PositiveSmallIntegerField(_("Priority"), validators=[MinValueValidator(1)])

    site = models.ForeignKey('Site', related_name='experiences', on_delete=models.CASCADE)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        """Meta definition for WorkExperience."""

        verbose_name = _('Work Experience')
        verbose_name_plural = _('Work Experiences')
        ordering = ('priority',)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(WorkExperience, self).save(*args, **kwargs)

class PortfolioCategory(models.Model):
    title = models.CharField(_("Title black"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 

    class Meta:
        verbose_name = _('Portfolio Category')
        verbose_name_plural = _('Portfolio Categories')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(PortfolioCategory, self).save(*args, **kwargs)

class Portfolio(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    subtitle = models.CharField(_("Subtitle"), max_length=60)
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Portfolio"), upload_to='site/Portfolio', height_field='height_field', width_field='width_field', max_length=100, validators=[FileExtensionValidator(['jpg', 'png'])])
    alternative_pic = models.CharField(_("Alternative Pic"), max_length=50)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Portfolio')
        verbose_name_plural = _('Portfolio')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(Portfolio, self).save(*args, **kwargs)

class Pricing(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    icon = models.FileField(_("Pricing"), upload_to='site/Pricing', max_length=100, validators=[FileExtensionValidator(['svg', 'png'])])
    
    per_unit = models.CharField(_("Per Unit"), max_length=100)
    price = models.PositiveIntegerField(_("Price"))
    priority = models.PositiveSmallIntegerField(_("Priority"), validators=[MaxValueValidator(3), MinValueValidator(1)])
    
    site = models.ForeignKey('Site', related_name='prices', on_delete=models.CASCADE)
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Pricing')
        verbose_name_plural = _('Pricing')
        ordering = ('priority',)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Pricing, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

class PriceFeature(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    disabled = models.BooleanField(_("Disabled"), default=False)
    
    pricing = models.ForeignKey('Pricing', related_name='features', on_delete=models.CASCADE)
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Price feature')
        verbose_name_plural = _('Price features')
        ordering = ('created',)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(PriceFeature, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

class Testimonial(models.Model):
    from_who = models.CharField(_("from who"), max_length=60)
    position = models.CharField(_("Position"), max_length=60)
    summary = RichTextField(_("Summary"))

    is_shown = models.BooleanField(_("Is shown"), default=False)

    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    pic = models.ImageField(_("Picture"), upload_to='site/Testimonial', height_field='height_field', width_field='width_field', max_length=100, validators=[FileExtensionValidator(['jpg', 'png'])])
    
    priority = models.PositiveSmallIntegerField(_("Priority"))

    site = models.ForeignKey('Site', related_name='testimonials', on_delete=models.CASCADE)
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    
    class Meta:
        verbose_name = _('Testimonial')
        verbose_name_plural = _('Testimonials')
        ordering = ('created',)

    def save(self, *args, **kwargs):
        super(Testimonial, self).save(*args, **kwargs)

    def __str__(self):
        return self.from_who

class Page(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True)
    
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    banner = models.ImageField(_("Banner"), upload_to='site/Banner', height_field='height_field', width_field='width_field', max_length=100, validators=[FileExtensionValidator(['jpg', 'png'])])
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')
        ordering = ('created',)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Page, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

class Contact(models.Model):

    first_name = models.CharField(_("First Name"), max_length=100, validators=[MinLengthValidator(3), MaxLengthValidator(100)])
    last_name = models.CharField(_("Last Name"), max_length=100, validators=[MinLengthValidator(3), MaxLengthValidator(100)])
    email = models.EmailField(_("Email"), max_length=254, validators=[EmailValidator])
    phone = models.CharField(_("Phone"), max_length = 13, validators=[MaxLengthValidator(13), MinLengthValidator(12)], null=True, blank=True)
    project = models.CharField(_("Project"), max_length=50)
    country = CountryField(_("Country"))
    message = models.TextField(_("Message"))
    created = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')
        ordering = ('-created',)

    # def form_invalid(self, form):
    #     """
    #         If the form is invalid, re-render the context data with the
    #         data-filled form and errors.
    #     """
    #     return self.render_to_response(self.get_context_data(form=form))

    def __str__(self):
        return self.first_name
    
    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)
    
    def get_email(self):
        return self.email
    
    # def get_phone_number(self):
    #     return self.phone
