from django.urls import path, re_path, include
from django.views.decorators.cache import cache_page
from . import views

app_name = 'pages'
urlpatterns = [
    re_path(r'^$', views.HomeView.as_view(), name='home'),
    re_path(r'^service/$', views.ServiceView.as_view(), name='service'),
    re_path(r'^about/$', views.AboutView.as_view(), name='about'),
    re_path(r'^contact/$', views.ContactView.as_view(), name='contact'),
    re_path(r'^portfolio/$', views.PortfolioView.as_view(), name='portfolio'),
    re_path(r'^portfolio/(?P<slug>[\w\-]+)/$', views.PortfolioDetailView.as_view(), name='project-detail'),
]
