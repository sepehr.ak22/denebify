from django.test import TestCase
from django.contrib.auth.models import User
from .forms import ContactForm

from pages.subtests.test_view_home import HomeViewTest
from pages.subtests.test_view_service import ServiceViewTest
from pages.subtests.test_view_about import AboutViewTest
from pages.subtests.test_view_contact import ContactViewTest
from pages.subtests.test_view_portfolio import PortfolioViewTest
from pages.subtests.test_view_portfoliodetail import PortfolioDetailViewTest

# Create your tests here.
class ContactFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True

    def test_invalid_min_length_phone_number(self):
        post_data = {
            'phone_number': '1234',
        }
        form = ContactForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='min length phone number SHOULD\'NT be valid.')

    def test_invalid_max_length_phone_number(self):
        post_data = {
            'phone_number': '12345678912345',
        }
        form = ContactForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='max length phone number SHOULD\'NT be valid.')

    def test_invalid_format_phone_number(self):
        post_data = {
            'phone_number': '911994103311',
        }
        form = ContactForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='format phone number SHOULD\'NT be valid.')
