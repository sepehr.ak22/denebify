from .base import (
    INSTALLED_APPS,
    STATIC_ROOT
)

from datetime import timedelta
from decouple import config

# ############## #
#   EXTENSIONS   #
# ############## #

INSTALLED_APPS.append('rest_framework')
INSTALLED_APPS.append('rest_framework.authtoken')
INSTALLED_APPS.append('rest_framework_swagger')
INSTALLED_APPS.append('rest_framework_simplejwt.token_blacklist',)

INSTALLED_APPS.append('django_filters')
INSTALLED_APPS.append('ckeditor')
INSTALLED_APPS.append('ckeditor_uploader')
INSTALLED_APPS.append('widget_tweaks')

INSTALLED_APPS.append('admin_interface')
INSTALLED_APPS.append('colorfield')

INSTALLED_APPS.append('corsheaders') 

INSTALLED_APPS.append('admin_honeypot')
INSTALLED_APPS.append('defender')
INSTALLED_APPS.append('compressor')
INSTALLED_APPS.append('debug_toolbar',)

INSTALLED_APPS.append('django.contrib.admindocs',)

INSTALLED_APPS.append('django_prometheus')

INSTALLED_APPS.append('hitcount')
INSTALLED_APPS.append('request')
INSTALLED_APPS.append('django_user_agents')
INSTALLED_APPS.append('markdown_deux')

# ############## #
# CUSTOM PROJECT #
# ############## #
INSTALLED_APPS.append('accounts')
INSTALLED_APPS.append('pages')
INSTALLED_APPS.append('project')
INSTALLED_APPS.append('step')
INSTALLED_APPS.append('ticket')
INSTALLED_APPS.append('blog')
INSTALLED_APPS.append('dashboard')

# #################### #
# IMPORTANT VARIABLES  #
# #################### #

HTML_MINIFY = True
KEEP_COMMENTS_ON_MINIFYING = True
SITE_ID = 1
DATA_UPLOAD_MAX_MEMORY_SIZE = 5242880


# #################### #
#       Compress       #
# #################### #
COMPRESS_ENABLED = True
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',  'compressor.filters.cssmin.CSSMinFilter']
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)


# CORS Setting
CORS_ORIGIN_WHITELIST = [
    "https://localhost:3000",
]


# ###################### #
# EXTENSION DEPENDENCIES #
# ###################### #

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend'],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    'PAGE_SIZE': 50
}

ALLOW_UNICODE_SLUGS = True

# ###################### #
# EXTENSION DEPENDENCIES #
# ###################### #

AUTHENTICATION_BACKENDS = [
    # Django ModelBackend is the default authentication backend.
    'django.contrib.auth.backends.ModelBackend',
]

LOGIN_REDIRECT_URL = 'pages:home'

LOGOUT_REDIRECT_URL = 'accounts:login'

# ######################### #
#         CKEDITOR          #
# ######################### #
CKEDITOR_UPLOAD_PATH = "ckeditor/"

CKEDITOR_CONFIGS = {
    'default': {
       'toolbar_Full': [
            ['Styles', 'Format', 'FontSize', 'Bold', 'Italic', 'Underline', 'Strike', 'SpellChecker', 'Undo', 'Redo'],
            ['Image', 'Table', 'HorizontalRule'],
            ['TextColor', 'BGColor'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['NumberedList','BulletedList'],
            ['Indent','Outdent'],
            ['Maximize',],
            
        ],
        'extraPlugins': 'justify,liststyle,indent',
   },
}

INTERNAL_IPS = ('127.0.0.1', )
DEBUG_TOOLBAR_PANELS = {
    'debug_toolbar.panels.profiling.ProfilingPanel',
}
# ######################### #
#         CKEDITOR          #
# ######################### #
LOGIN_REDIRECT_URL = '/dashboard'
LOGIN_URL = '/accounts/login'
LOGOUT_REDIRECT_URL = '/'



# ######################### #
#            JWT            #
# ######################### #
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=14),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': True,   
    'ALGORITHM': 'HS256',
    'SIGNING_KEY': config('SECRET_KEY'),
    'VERIFYING_KEY': None,

    'AUTH_HEADER_TYPES': ('DENEB',),

    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',
    
    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',
}

JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
    'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
    'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
    'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
    'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
    'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_SECRET_KEY': config('SECRET_KEY'),
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': timedelta(seconds=300),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': False,
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'DENEB',
    'JWT_AUTH_COOKIE': None,

}


# #################### #
#    HitCount Config   #
# #################### #

HITCOUNT_KEEP_HIT_ACTIVE = {'minutes': 60}
HITCOUNT_HITS_PER_IP_LIMIT = 0  # unlimited
HITCOUNT_EXCLUDE_USER_GROUP = ()  # not used
HITCOUNT_KEEP_HIT_IN_DATABASE = {'seconds': 10}