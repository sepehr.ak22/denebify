from django.contrib.auth.models import User

from django.dispatch import receiver

from django.db.models.signals import post_save

from django.core.mail import send_mail

from django.conf import settings

from accounts.models import (
    Profile,
)

from project.models import (
    Application,
    Member,
    Team
)

from step.models import (
    Step,
)

from project.models import (
    Project,
)

@receiver(post_save, sender=Profile)
def change_state_to_complete(sender, instance, created, **kwargs):

    if created:

        apps = Application.objects.filter(user = instance.user)

        if not apps:
    
            Application.objects.create(user = instance.user)

    if instance.is_published:
        application_obj = Application.objects.get(user = instance.user)
        
        profile_step = Step.objects.get(user = instance.user, step = '1')
        profile_step.state = 'c'
        profile_step.save()

        application_step = Step.objects.get(user = instance.user, step = '2')
        application_step.state = 'i'
        application_step.application = application_obj
        application_step.save()

        steps = Step.objects.filter(user = instance.user,
                                    step__in = ['3', '4', '5', '6']
                                )

        for step in steps:
            step.application = application_obj
            step.save()

        
@receiver(post_save, sender=Application)
def change_state(sender, instance, created, **kwargs):
    # import pdb ; pdb.set_trace()
    if created:

        count = Application.objects.filter(user=instance.user).count()

        if count > 1:
            
            Step.objects.create(user = instance.user,
                                application = instance,
                                state = 'i',
                                step = '2'
                            )

            for i in range(3,7):
                
                Step.objects.create(user = instance.user,
                                    application = instance,
                                    state = 't',
                                    step = str(i)
                                )

        else:
            
            app = Application.objects.get(user=instance.user)

            Step.objects.create(user = instance.user,
                                state = 'i',
                                step = '1'
                               )

            for i in range(2,7):
                
                Step.objects.create(user = instance.user,
                                    application = instance,
                                    state = 't',
                                    step = str(i)
                                )

            # import pdb ; pdb.set_trace()
    
    if instance.is_published:
        application_step = Step.objects.get(user = instance.user,
                                            application = instance,
                                            step = '2'
                                           )
        application_step.state = 'c'
        application_step.save()

        Project.objects.create(application = instance)


@receiver(post_save, sender=Project)
def change_state_meeting(sender, instance, created, **kwargs):
    if created:

        project_step = Step.objects.get(user = instance.application.user,
                                        application = instance.application,
                                        step = '3'
                                       )
        project_step.state = 'i'
        project_step.save()

    if instance.is_published and not instance.is_shown:

        project_step = Step.objects.get(user = instance.application.user,
                                        application = instance.application,
                                        step = '3'
                                       )
        project_step.state = 'c'
        project_step.save()

        team_step = Step.objects.get(user = instance.application.user,
                                     application = instance.application,
                                     step = '4'
                                    )

        team_step.state = 'i'
        team_step.save()

    if instance.is_published and instance.is_shown:

        project_step = Step.objects.filter(user = instance.application.user,
                                           application = instance.application,
                                           step = '3',
                                           state = 'c'
                                          )

        if not bool(project_step):

            project_step[0].state = 'c'
            project_step[0].save()

        team_step = Step.objects.filter(user = instance.application.user,
                                        application = instance.application,
                                        step = '4',
                                        state = 't'
                                       )
        
        if bool(team_step):

            team_step[0].state = 'i'
            team_step[0].save()
        
        preview_step = Step.objects.get(user = instance.application.user,
                                        application = instance.application,
                                        step = '5'
                                       )

        preview_step.state = 'c'
        preview_step.save()

        result_step = Step.objects.get(user = instance.application.user,
                                       application = instance.application,
                                       step = '6'
                                      )

        result_step.state = 'i'
        result_step.save()
        
@receiver(post_save, sender=Team)
def change_state_team(sender, instance, created, **kwargs):
    # import pdb ; pdb.set_trace()
    if instance.is_published:

        team_step = Step.objects.get(user = instance.project.application.user,
                                     application = instance.project.application,
                                     step = '4'
                                    )
        team_step.state = 'c'
        team_step.save()

        preview_step = Step.objects.get(user = instance.project.application.user,
                                        application = instance.project.application,
                                        step = '5'
                                       )

        preview_step.state = 'i'
        preview_step.save()