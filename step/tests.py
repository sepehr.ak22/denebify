import os

from django.conf import settings

from django.test import TestCase

from django.contrib.auth.models import User

from django.core.files.uploadedfile import SimpleUploadedFile

from step.models import Step

from project.models import (
    Application,
    Project,
    Team,
    Member
)


class StepValidModelTest(TestCase):
    
    def setUp(self):
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True
    
    
    def __create_draft_profile(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()

                self.user.profile.origin = 'US'
                self.user.profile.residence = 'US'
                self.user.profile.phone_number = '+989119941033'
                self.user.profile.gender = 'm'
                self.user.profile.bio = 'lorem ipsum'
                self.user.profile.location = 'Tehran, Iran'
                self.user.profile.social_media = 'instagram.com/sepehr.ak'
                self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                self.user.profile.save()

    
    def __create_published_profile(self):
        self.user.profile.is_published = True
        self.user.profile.save()
        
    
    def __create_draft_application(self):
        data = {
            'user': self.user,
            'qatari_partnership': False,
            'kafil_fname': 'sepehr',
            'kafil_lname': 'akbarzadeh',
            'kafil_email': 'sepehr.ak@gmail.com',
            'kafil_qatari_id': '1111111111111',
            'website': 'deneb.com',
            'industry': 'Art',
            'deneb_result': 'Techshow',
            'abstract': 'lorem ipsum',
            'is_b2b': True,
            'is_b2c': False,
            'is_b2g': False,
            'is_b2b2c': False,
            'has_product': True,
            'sales_rate': '10000000',
            'sales_rate_type': 'm',
            'keywords': 'sepehr, akbarzadeh, abkenar, nowshahr'
        }

        app = Application.objects.create(**data)
        return app
    
    
    def __create_published_application(self, app):
        app.is_published = True
        app.save()
        return app
     
    
    def __create_draft_project(self, app):
        
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:
            with open(os.path.join(settings.BASE_DIR, 'painless/videos/Fleeting.mp4'), 'rb') as uploaded_video:
                
                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()
                uploaded_video_name = uploaded_video.name
                uploaded_video_file = uploaded_video.read()
                
                app.project.title = 'deneb'
                app.project.summary = 'lorem ipsum'
                app.project.product = 'lorem ipsum'
                app.project.customer = 'lorem ipsum'
                app.project.business = 'lorem ipsum'
                app.project.market = 'lorem ipsum'
                app.project.competition = 'lorem ipsum'
                app.project.vision = 'lorem ipsum'
                app.project.founders = 'lorem ipsum'
                app.project.cover = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.video = SimpleUploadedFile(uploaded_video_name, uploaded_video_file)
                app.project.summary_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.product_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.customer_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.business_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.market_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.competition_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.vision_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.founders_img = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                app.project.is_published = False
                
                app.project.save()

    
    def __create_published_project(self, project):
        
        project.is_published = True
        project.save()
        return project

    
    def __create_team(self, project):

        data = {
            'name': 'SAGE',
            'user': self.user,
            'project': project,
            'is_published': False
        }

        self.team = Team.objects.create(**data)
    
    
    def __create_team_member(self):

        data = {
            'first_name': 'sepehr',
            'last_name': 'akbarzade',
            'position': 'CEO',
            'phone_number': '+0989119941033',
            'email': 'sepehr.ak@gmail.com',
            'team': self.team
        }

        self.team_member = Member.objects.create(**data)

    
    def __create_published_team(self):

        self.team.is_published = True

        self.team.save()

    
    def __create_is_shown_project(self, project):

        project.is_shown = True
        
        project.save()

    
    def test_step_creation(self):
        self.assertIsNotNone(self.user.steps, msg='signal step creation doesn\'t work.')

    
    def test_6_number_step_creation(self):
        self.assertEqual(6, len(self.user.steps.all()), msg='signal step doesn\'t create 6 number of steps for each user app.')

    
    def test_first_step_state(self):

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                application = None
                            ).exists()
                            
        self.assertTrue(
            profile_step,
            msg = 'profile step must be created'
            )

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                state = 'i',
                                application = None
                            ).exists()

        self.assertTrue(
            profile_step,
            msg = 'state of profile step must be in-progress'
        )

        count = Application.objects.filter(user = self.user).count()

        self.assertNotEqual(
            count,
            0,
            msg = 'There must be a application'
        )

        self.assertEqual(
            count,
            1,
            msg = 'There should only be one application'
        )

        app = Application.objects.filter(user = self.user)[0]

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 't',
                                application = app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} should be an task state'.format(step)
                )

    
    def test_draft_profile_step(self):

        self.__create_draft_profile()

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                application = None
                            ).exists()
                            
        self.assertTrue(
            profile_step,
            msg = 'profile step must be created'
            )

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                state = 'i',
                                application = None
                            ).exists()

        self.assertTrue(
            profile_step,
            msg = 'state of profile step must be in-progress'
        )

        count = Application.objects.filter(user = self.user).count()

        self.assertNotEqual(
            count,
            0,
            msg = 'There must be a application'
        )

        self.assertEqual(
            count,
            1,
            msg = 'There should only be one application'
        )

        app = Application.objects.filter(user = self.user)[0]

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 't',
                                application = app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} should be an task state'.format(step)
                )

    
    def test_published_profile_step(self):
        
        self.__create_draft_profile()

        self.__create_published_profile()

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                application = None
                            ).exists()
                            
        self.assertTrue(profile_step, msg = 'profile step should have existed')

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                state = 'c',
                                application = None
                            ).exists()
        self.assertTrue(profile_step, msg = 'state of profile step must be completed')

        count = Application.objects.filter(user = self.user).count()

        self.assertNotEqual(
            count,
            0,
            msg = 'There must be at least one application'
        )

        app = Application.objects.get(user = self.user)

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 't' if step != '2' else 'i',
                                application = app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be an {} state'.format(
                                                                    step,
                                                                    app,
                                                                    'task' if step != 2 else 'in-progress',
                                                                )
            )

    
    def test_draft_application_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                application = None
                            ).exists()
                            
        self.assertTrue(profile_step, msg = 'profile step should have existed')

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                state = 'c',
                                application = None
                            ).exists()

        self.assertTrue(profile_step, msg = 'state of profile step must be completed')

        count = Application.objects.filter(user = self.user).count()

        self.assertNotEqual(
            count,
            0,
            msg = 'There must be at least one application'
        )

        new_app = self.__create_draft_application()

        apps = Application.objects.filter(user = self.user)

        for app in apps:

            for step in list(map(str, range(2,7))):

                desired_step = Step.objects.filter(
                                    user = self.user,
                                    step = step,
                                    state = 't' if step != '2' else 'i',
                                    application = app
                                ).exists()

                self.assertTrue(
                    desired_step,
                    msg = 'Step {} of \'{}\'should be an {} state'.format(
                                                                    step,
                                                                    app,
                                                                    'task' if step != '2' else 'in-progress',
                                                                )
                )

    
    def test_published_application_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                application = None
                            ).exists()
                            
        self.assertTrue(profile_step, msg = 'profile step should have existed')

        profile_step = Step.objects.filter(
                                user = self.user,
                                step = 1,
                                state = 'c',
                                application = None
                            ).exists()

        self.assertTrue(profile_step, msg = 'state of profile step must be completed')

        count = Application.objects.filter(user = self.user).count()

        self.assertNotEqual(
            count,
            0,
            msg = 'There must be at least one application'
        )

        apps = Application.objects.filter(user = self.user)

        for app in apps:

            for step in list(map(str, range(2,7))):

                desired_step = Step.objects.filter(
                                    user = self.user,
                                    step = step,
                                    state = 't' if step != '2' else 'i',
                                    application = app
                                ).exists()

                self.assertTrue(
                    desired_step,
                    msg = 'Step {} of \'{}\'should be an {} state'.format(
                                                                        step,
                                                                        app,
                                                                        'task' if step != '2' else 'in-progress',
                                                                    )
                )

        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step == '2' else ('i' if step == '3' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step == '2' else (
                                                                        'an In-progress' if step == '3' else 'a Task'
                                                                    ),
                                                            )
            )

    
    def test_draft_project_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()
        
        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step == '2' else ('i' if step == '3' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step == '2' else (
                                                                        'an In-progress' if step == '3' else 'a Task'
                                                                    ),
                                                            )
            )

        self.__create_draft_project(new_published_app)
        
        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step == '2' else ('i' if step == '3' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step == '2' else (
                                                                        'an In-progress' if step == '3' else 'a Task'
                                                                    ),
                                                            )
            )


    def test_published_project_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()
        
        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        self.__create_draft_project(new_published_app)
        
        published_project = self.__create_published_project(new_published_app.project)

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step in ['2', '3'] else ('i' if step == '4' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step in ['2', '3'] else (
                                                                        'an In-progress' if step == '4' else 'a Task'
                                                                    ),
                                                            )
            )

  
    def test_team_project_without_member_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()
        
        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        self.__create_draft_project(new_published_app)
        
        published_project = self.__create_published_project(new_published_app.project)

        self.__create_team(published_project)

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step in ['2', '3'] else ('i' if step == '4' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step in ['2', '3'] else (
                                                                        'an In-progress' if step == '4' else 'a Task'
                                                                    ),
                                                            )
            )

 
    def test_team_project_with_member_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()
        
        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        self.__create_draft_project(new_published_app)
        
        published_project = self.__create_published_project(new_published_app.project)

        self.__create_team(published_project)

        self.__create_team_member()

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step in ['2', '3'] else ('i' if step == '4' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step in ['2', '3'] else (
                                                                        'an In-progress' if step == '4' else 'a Task'
                                                                    ),
                                                            )
            )


    def test_published_team_project_with_member_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()
        
        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        self.__create_draft_project(new_published_app)
        
        published_project = self.__create_published_project(new_published_app.project)

        self.__create_team(published_project)

        self.__create_team_member()

        self.__create_published_team()

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'c' if step in ['2', '3', '4'] else ('i' if step == '5' else 't'),
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'a Complete' if step in ['2', '3', '4'] else (
                                                                        'an In-progress' if step == '5' else 'a Task'
                                                                    ),
                                                            )
            )


    def test_is_shown_project_step(self):

        self.__create_draft_profile()

        self.__create_published_profile()
        
        new_app = self.__create_draft_application()

        new_published_app = self.__create_published_application(new_app)

        self.__create_draft_project(new_published_app)
        
        published_project = self.__create_published_project(new_published_app.project)

        self.__create_team(published_project)

        self.__create_team_member()

        self.__create_published_team()

        self.__create_is_shown_project(published_project)

        for step in list(map(str, range(2,7))):

            desired_step = Step.objects.filter(
                                user = self.user,
                                step = step,
                                state = 'i' if step == '6' else 'c', 
                                application = new_published_app
                            ).exists()

            self.assertTrue(
                desired_step,
                msg = 'Step {} of \'{}\'should be {} state'.format(
                                                                step,
                                                                new_published_app.sku,
                                                                'an In-progress' if step == '6' else 'a Complete'
                                                            )
            )