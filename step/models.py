from django.db import models
from django.core.validators import MinLengthValidator
from django.core.validators import MaxLengthValidator
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.conf import settings

from step.managers import StepManager

class Step(models.Model):
    SECTIONS =( 
        ("1", "Profile"), 
        ("2", "Application"), 
        ("3", "Project"), 
        ("4", "Team"), 
        ("5", "Preview"), 
        ("6", "Result"), 
    ) 
    STATES =( 
        ("t", "task"), 
        ("i", "in_Progress"), 
        ("c", "Complete"), 
    ) 

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name = _("User"),
                             related_name = 'steps',
                             on_delete = models.PROTECT
                            )

    state = models.CharField(max_length = 1, choices = STATES, validators=[MinLengthValidator(1), MaxLengthValidator(1)], default = 'i')
    step = models.CharField(max_length = 1, choices = SECTIONS, validators=[MinLengthValidator(1), MaxLengthValidator(1)], default = '1')

    application = models.ForeignKey('project.Application',
                                    verbose_name = _("Application"), 
                                    related_name = 'steps',
                                    on_delete = models.PROTECT,
                                    blank = True, null = True
                                   )

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = StepManager()

    class Meta:
        verbose_name = _('Step')
        verbose_name_plural = _('Steps')
        ordering = ('step',)

    def __str__(self):
        return f'<{self.state}>  <{self.user.username}>'

    def __repr__(self):
        return f'<{self.state}>  <{self.user.username}>'