from django.apps import AppConfig


class StepConfig(AppConfig):
    name = 'step'


    def ready(self):
        from step import signals