from django.db import models

from project.models import (
    Application,
    Project
)


class StepQueryset(models.QuerySet):

    def whole_process(self, app):

        result = (self.filter(
            application = app, 
            state = 'c'
        ).count() / 5) * 100
        
        return app.sku, result


    def profile_status(self, user):

        return self.get(
            user = user,
            step = '1'
        )


    def application_status(self, app):

        return self.get(
            application = app,
            step = '2'
        )


    def project_status(self, project):

        return self.get(
            application = project.application,
            step = '3'
        )


    def application_steps_with_c_state(self, app):

        return self.filter(
            application = app,
            state = 'c'
        )


class StepManager(models.Manager):

    # Returns True if all application steps is completed
    def __get_application_status(self, app):

        number_of_complete_application_steps = self.get_queryset().application_steps_with_c_state(app).count()

        return True if number_of_complete_application_steps == 5 else False


    def get_queryset(self):

        return StepQueryset(self.model, using = self._db)  # Important!


    def whole_process(self, user):

        apps = Application.objects.filter(user = user)

        return list(map(lambda app: self.get_queryset().whole_process(app), apps))

    
    def profile_status(self, user):

        status = self.get_queryset().profile_status(user)

        return True if status.state == 'c' else False

    
    def application_status(self, app):

        status = self.get_queryset().application_status(app)

        return True if status.state == 'c' else False

    
    def project_status(self, project):

        status = self.get_queryset().project_status(project)

        return True if status.state == 'c' else False

    
    def completed_applications_count(self, user):

        apps = Application.objects.filter(user = user)

        count = 0

        for app in apps:

            if self.application_status(app): count += 1

        return count

    
    def completed_projects_count(self, user):

        projects = Project.objects.projects(user)

        count = 0

        for project in projects:

            if self.project_status(project): count += 1

        return count

    
    # List of applications that have not completed all steps +
    def inprogress_requests(self, user):

        apps = Application.objects.filter(user = user)

        inprogress_requests = list()

        for app in apps:

            if not self.__get_application_status(app):
                
                inprogress_requests.append(app)

        return inprogress_requests


    # Number of applications that have not completed all steps +
    # profile if is not completed
    def inprogress_requests_count(self, user):

        count = len(self.inprogress_requests(user))

        if not self.profile_status(user): count + 1

        return count

    
    # List of applications that is completed all steps +
    def completed_requests(self, user):

        apps = Application.objects.filter(user = user)

        completed_requests = list()

        for app in apps:

            if self.__get_application_status(app):
                
                completed_requests.append(app)

        return completed_requests


    # Number of applications that is completed all steps +
    # profile if is completed
    def completed_requests_count(self, user):

        count = len(self.completed_requests(user))

        if self.profile_status(user): count + 1

        return count

    
    def percentage_of_completed_applications(self, user):

        total = Application.objects.applications_count(user)

        completed_applications_count = self.completed_applications_count(user)

        return (completed_applications_count / total) * 100


    def percentage_of_completed_projects(self, user):

        total = Project.objects.projects_count(user)

        if total == 0: return 0

        completed_projects_count = self.completed_projects_count(user)

        return (completed_projects_count / total) * 100