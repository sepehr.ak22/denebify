from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField
from django.urls import reverse
from django.utils.text import slugify
from painless.upload_features.upload_path import blog_upload_to

class Category(models.Model):
    title = models.CharField(_("Title"), max_length=60,unique = True , db_index = True)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Post Category')
        verbose_name_plural = _('Post Categories')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("blog:category", kwargs={"slug": self.slug})
    def get_queryset(self):

        self.model.objects.all()


class Post(models.Model):

    author = models.ForeignKey(User, related_name='articles', on_delete=models.CASCADE)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True) 
    title = models.CharField(max_length=256, db_index=True )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    main_text = RichTextField()
    width_field  = models.PositiveIntegerField(null = True , blank = True , default = '1080')
    height_field = models.PositiveIntegerField(null = True , blank = True , default = '720')
    image = models.ImageField(upload_to=blog_upload_to,blank=True, null=True , help_text='uplaod image',width_field='width_field', height_field='height_field')
    category = models.ForeignKey(Category, related_name='articles', on_delete = models.SET_NULL , null =True)
    
    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('blog_post', kwargs={'slug': slug})