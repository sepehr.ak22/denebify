# Generated by Django 3.0.8 on 2020-08-09 15:44

import ckeditor.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import painless.upload_features.upload_path


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=60, unique=True, verbose_name='Title')),
                ('slug', models.SlugField(allow_unicode=True, editable=False, max_length=60, verbose_name='Slug')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
            ],
            options={
                'verbose_name': 'Post Category',
                'verbose_name_plural': 'Post Categories',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(allow_unicode=True, editable=False, max_length=60, verbose_name='Slug')),
                ('title', models.CharField(db_index=True, max_length=256)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('main_text', ckeditor.fields.RichTextField()),
                ('width_field', models.PositiveIntegerField(blank=True, default='1080', null=True)),
                ('height_field', models.PositiveIntegerField(blank=True, default='720', null=True)),
                ('image', models.ImageField(blank=True, height_field='height_field', help_text='uplaod image', null=True, upload_to=painless.upload_features.upload_path.blog_upload_to, width_field='width_field')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='articles', to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='articles', to='blog.Category')),
            ],
            options={
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
        ),
    ]
