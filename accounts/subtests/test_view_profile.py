import os

from django.test import TestCase

from django.conf import settings

from django.urls import reverse

from django.contrib.auth.models import User

from django.core.files.uploadedfile import SimpleUploadedFile

from project.models import Application


class AccountProfileUpdateViewTest(TestCase):
    
    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

        self.user.profile.email_confirmed = True

        self.user.profile.save()

    
    def test_without_login(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            data = {
                'origin': 'US',
                'residence': 'US',
                'phone_number': '+989119941033',
                'gender': 'm',
                'bio': 'lorem ipsum',
                'location': 'Tehran, Iran',
                'social_media': 'instagram.com/sepehr.ak',
                'id_card': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                'picture': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
            }

        app = Application.objects.get(user = self.user)

        response = self.client.post(
            reverse(
                'dashboard:account-profile-edit',
                kwargs = {
                    'sku_app': app.sku,
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:account-profile-edit',
                    kwargs = {
                        'sku_app': app.sku,
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )


    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            data = {
                'origin': 'US',
                'residence': 'US',
                'phone_number': '+989119941033',
                'gender': 'm',
                'bio': 'lorem ipsum',
                'location': 'Tehran, Iran',
                'social_media': 'instagram.com/sepehr.ak',
                'id_card': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                'picture': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
            }
        
        app = Application.objects.get(user = self.user)

        response = self.client.post(
            reverse(
                'dashboard:account-profile-edit',
                kwargs = {
                    'sku_app': app.sku,
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )
        
        self.assertIn(
            b'Your profile draft has been changed.',
            response.content
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )


class AccountProfileFinalizeViewTest(TestCase):
    
    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
        )

        self.user.set_password('h@med_1234')

        self.user.save()

        self.user.profile.email_confirmed = True

        self.user.profile.save()


    def __create_draft_profile(self):

        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            self.user.profile.origin = 'US'
            self.user.profile.residence = 'US'
            self.user.profile.phone_number = '+989119941033'
            self.user.profile.gender = 'm'
            self.user.profile.bio = 'lorem ipsum'
            self.user.profile.location = 'Tehran, Iran'
            self.user.profile.social_media = 'instagram.com/sepehr.ak'
            self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
            self.user.profile.save()


    def test_without_login(self):

        data = {
            'is_published': True
        }

        app = Application.objects.get(user = self.user)

        response = self.client.post(
            reverse(
                'dashboard:account-profile-published',
                kwargs = {
                    'sku_app': app.sku,
                }
            ),
            data = data
        )

        self.assertRedirects (
            response,
            '/accounts/login?next={}'.format(
                reverse(
                    'dashboard:account-profile-published',
                    kwargs = {
                        'sku_app': app.sku,
                    }
                )
            ),
            status_code = 302,
            target_status_code =  301,
            msg_prefix = "The user must first log in.",
            fetch_redirect_response = True
        )

    
    def test_valid_data(self):

        login_flag = self.client.login(username='hamed_test', password='h@med_1234')

        self.assertTrue(
            login_flag,
            msg = 'You could not log in.'
        )

        self.__create_draft_profile()

        data = {
            'is_published': True
        }

        app = Application.objects.get(user = self.user)

        response = self.client.post(
            reverse(
                'dashboard:account-profile-published',
                kwargs = {
                    'sku_app': app.sku,
                }
            ),
            data = data,
            follow = True
        )

        self.assertEqual(
            response.status_code,
            200
        )
        
        self.assertIn(
            b'Your profile is finalized.',
            response.content
        )

        self.assertRedirects (
            response,
            reverse(
                'dashboard:application-edit',
                kwargs = {
                    'sku': app.sku,
                }
            ),
            status_code = 302,
            target_status_code =  200,
            msg_prefix = "redirect is invalid",
            fetch_redirect_response = True
        )