import re

from django import forms

from django.utils.translation import ugettext_lazy as _

from django.core.files.images import get_image_dimensions

from django.core import validators

from django.contrib.auth.models import User

from django.contrib.auth.forms import UserCreationForm

from django_countries.widgets import CountrySelectWidget

from accounts.models import Profile

from painless.regex.patterns import INTERNATIONAL_PHONE_NUMBER_PATTERN

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

class ProfileDraftForm(forms.ModelForm):


    def __init__(self, *args, **kwargs):
        super(ProfileDraftForm, self).__init__(*args, **kwargs)
        self.user = kwargs['instance']
    
    class Meta:

        model = Profile

        fields = ('id_card', 'phone_number',
                  'picture', 'is_published', 'origin',
                  'residence', 'gender', 'bio', 'location',
                  'social_media',
                 )

        widgets = {'origin': CountrySelectWidget(),
                   'residence': CountrySelectWidget()
                  }

    def clean_phone_number(self):
        data = self.cleaned_data['phone_number']
        if data:
            if not re.match(INTERNATIONAL_PHONE_NUMBER_PATTERN, data):
                raise forms.ValidationError(_('Invalid value: %(value)s'), code='invalid', params={'value': data})
        return data

    def clean_gender(self):
        data = self.cleaned_data['gender']
        if data:
            if data not in ['m', 'f', 'o']:
                raise forms.ValidationError(_('Invalid value: %(value)s'), code='invalid', params={'value': data})
        return data

    def clean_picture(self):
        data = self.cleaned_data.get("picture")
        if data:
            w, h = get_image_dimensions(data)
            if w != h:
                raise forms.ValidationError(_('picture ratio must be equal: The is %(value)s'), code='invalid', params={'value': (w, h)})
        return data

    def clean(self):

        # super().clean() ensures that any validation logic in parent classes is maintained
        cleaned_data = super().clean()
        is_published = cleaned_data.get('is_published')

        email_confirmed = self.user.profile.email_confirmed

        if not email_confirmed:
            raise forms.ValidationError(_('Your email has not confirmed!'))

        if is_published:
            raise forms.ValidationError(_('You cannot submit form when it is published already.'), code='forbidden')
        
        return cleaned_data

class ProfilePublishForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProfilePublishForm, self).__init__(*args, **kwargs)
        self.user = kwargs['instance']

    class Meta:
        model = Profile

        fields = ( 'is_published', )

        widgets = {
            'is_published': forms.HiddenInput(),
        }

    def clean(self):

        # super().clean() ensures that any validation logic in parent classes is maintained
        cleaned_data = super().clean()
        is_published = cleaned_data.get('is_published')
        email_confirmed = self.user.profile.email_confirmed

        if not email_confirmed:
            raise forms.ValidationError(_('Your email has not confirmed!'))

        if is_published:
            origin = self.user.profile.origin
            residence = self.user.profile.residence
            phone_number = self.user.profile.phone_number
            gender = self.user.profile.gender
            bio = self.user.profile.bio
            location = self.user.profile.location
            social_media = self.user.profile.social_media
            id_card = self.user.profile.id_card
            picture = self.user.profile.picture
            
            if not phone_number:
                raise forms.ValidationError(_('`Phone Number` cannot be none.'), code='invalid')

            if not origin:
                raise forms.ValidationError(_('`Country Origin` cannot be none.'), code='invalid')
            
            if not residence:
                raise forms.ValidationError(_('`Country Residence` cannot be none.'), code='invalid')

            if not gender:
                raise forms.ValidationError(_('`Gender` cannot be none.'), code='invalid')
                
            if not bio:
                raise forms.ValidationError(_('`Bio` cannot be none.'), code='invalid')
                
            if not location:
                raise forms.ValidationError(_('`Location` cannot be none.'), code='invalid')
        
            if not social_media:
                raise forms.ValidationError(_('`Social Media` cannot be none.'), code='invalid')

            if not id_card:
                raise forms.ValidationError(_('`Qid or Passport` cannot be none.'), code='invalid')
            
            if not picture:
                raise forms.ValidationError(_('`Picture` cannot be none.'), code='invalid')
