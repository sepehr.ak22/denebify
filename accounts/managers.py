from django.db import models


class ProfileQuerySet(models.QuerySet):

    def profile(self, user):

        return self.get(user = user)

    
class ProfileManager(models.Manager):

    def __get_profile_progress(self, profile):

        profile_progress_num = 11

        attrs = vars(profile)
        
        for key in attrs.keys():
            
            if key in [
                        'email_confirmed',
                        'id_card',
                        'phone_number',
                        'picture',
                        'is_published',
                        'origin',
                        'residence',
                        'gender',
                        'bio',
                        'location',
                        'social_media']:

                if not attrs[key]:

                    profile_progress_num -= 1

            else:

                continue

        percent_complete_of_profile = (profile_progress_num / 11) * 100
      
        return percent_complete_of_profile


    def get_queryset(self):

        return ProfileQuerySet(self.model, using = self._db)  # Important!


    def profile(self, user):

        return self.get_queryset().profile(user)

    
    def profile_progress(self, user):

        return self.__get_profile_progress(self.profile(user))