from django.views.generic import View

from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import (
    redirect, 
    render,
    get_object_or_404
)

from django.contrib import messages

from django.urls import reverse_lazy

from accounts.models import Profile

from accounts.forms import (
    ProfileDraftForm,
    ProfilePublishForm
)


class AccountProfileUpdateView(LoginRequiredMixin, View):

    template_name = 'dashboard/pages/account-application-edit.html'

    view = {
        'page_name': "Profile"
    }

    form = ProfileDraftForm


    def is_user_valid(self, request, instance, *args, **kwargs):

        return True if request.user.pk == instance.user.pk else False


    def post(self, request, sku_app=None, *args, **kwargs):
        
        profile = Profile.objects.get(user = request.user)

        if self.is_user_valid(request, profile):

            form = self.form(
                request.POST,
                request.FILES,
                instance=request.user
            )

            if form.is_valid():

                profile.cover = form.cleaned_data.get('cover')
                profile.id_card = form.cleaned_data.get('id_card')
                profile.picture = form.cleaned_data.get('picture')
                profile.phone_number = form.cleaned_data.get('phone_number')
                profile.origin = form.cleaned_data.get('origin')
                profile.gender = form.cleaned_data.get('gender')
                profile.residence = form.cleaned_data.get('residence')
                profile.bio = form.cleaned_data.get('bio')
                profile.location = form.cleaned_data.get('location')
                profile.instagram = form.cleaned_data.get('instagram')
                profile.facebook = form.cleaned_data.get('facebook')
                profile.twitter = form.cleaned_data.get('twitter')
                profile.save()

                messages.success(request, "Your profile draft has been changed.")

                return redirect(
                    reverse_lazy(
                        'dashboard:application-edit',
                        kwargs={"sku":sku_app}
                    ),
                    context = {"form": form, "view": self.view }
                )

            else:
                import pdb ; pdb.set_trace()
                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = 'Qid or Passport' if f == 'id_card' else ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                    messages.error(request, '{}: {}'.format(field, message))

                return redirect(
                    reverse_lazy(
                        'dashboard:application-edit',
                        kwargs={"sku":sku_app}
                    ),
                    context = {"form": form, "view": self.view }
                )

        else:

            messages.error(request, "Some Error has been occured.")

            return redirect('dashboard:application-edit', sku_app)


class AccountProfileFinalizeView(LoginRequiredMixin, View):

    view = {
        'page_name': "Profile"
    }
    form = ProfilePublishForm

   
    def is_user_valid(self, request, user, *args, **kwargs):
        return True if request.user.pk == user.pk else False


    def post(self, request, sku_app=None, *args, **kwargs):

        profile = Profile.objects.get(user = request.user)

        if self.is_user_valid(request, profile):

            form = self.form(request.POST, instance = request.user)

            if form.is_valid():

                profile.is_published = True

                profile.save()

                messages.success(request, "Your profile is finalized.")

                return redirect('dashboard:application-edit', sku_app)
            
            else:
                import pdb ; pdb.set_trace()
                for f, m in form.errors.get_json_data().items():

                        message = m[0]['message']

                        field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   
                        
                        messages.error(request, '{}: {}'.format(field, message))
                
                return redirect("dashboard:application-edit", sku_app)
        else:

            messages.error(request, "Some Error has been occured.")

            return redirect('dashboard:application-edit', sku_app)