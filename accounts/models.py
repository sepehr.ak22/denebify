from django.db import models

from django.core.validators import (
    URLValidator,
    RegexValidator,
    MinLengthValidator,
    MaxLengthValidator,
    FileExtensionValidator,
)

from django.core.exceptions import ValidationError

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from django_countries.fields import CountryField

from painless.upload_features.upload_path import images_upload_to

from accounts.managers import ProfileManager


class Profile(models.Model):
    GENDERS = (
        ('m', _('Male')),
        ('f', _('Female')),
        ('o', _('Other')),
    )
    user = models.OneToOneField(User, related_name = 'profile', on_delete=models.CASCADE)
    
    id_card = models.FileField(verbose_name='Qid or Passport',
                               upload_to=images_upload_to,
                               null = True, blank = True, 
                               validators=[FileExtensionValidator(allowed_extensions=['pdf', 'jpg', 'png', 'JPG', 'jpeg', 'JPEG'])]
                              )


    phone_number = models.CharField(_("Phone Number"), max_length = 13, validators=[MaxLengthValidator(13), MinLengthValidator(12)], null=True, blank=True)
    
    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    picture = models.ImageField(_("Picture"), 
                                upload_to='site', 
                                height_field='height_field', 
                                width_field='width_field', 
                                validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png', 'JPG', 'jpeg', 'JPEG'])],
                                max_length=100, 
                                null = True, 
                                blank = True)
    
    is_published = models.BooleanField(default = False)

    origin = CountryField(_("Country Origin"), null = True, blank = True)
    residence = CountryField(_("Country Residence"), null = True, blank = True)
    gender = models.CharField(_("Gender"), choices = GENDERS, max_length = 1, null = True, blank = True)
    bio = models.TextField(max_length=650, null = True, blank = True)
    location = models.CharField(_("Address"), max_length = 120, null = True, blank = True)
    email_confirmed = models.BooleanField(default=False)

    social_media = models.URLField(_("Social Media"), max_length=200, validators=[URLValidator], blank = True, null = True)

    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = ProfileManager()
    
    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def __str__(self):
        return self.user.get_full_name()

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)
