import os

from django.test import (
    TestCase,
    Client
)

from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile

from django.conf import settings

from accounts.models import Profile

from accounts.forms import ProfileDraftForm
from accounts.forms import ProfilePublishForm

from accounts.subtests.test_view_profile import (
    AccountProfileUpdateViewTest,
    AccountProfileFinalizeViewTest
)


class ProfileModelTest(TestCase):
    def setUp(self):
        # create new user
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')

    def test_user_has_profile(self):
        # check user has profile
        self.assertIsNotNone(self.user.profile, msg='signal profile creation doesn\'t work.')


class ProfileValidDraftFormTest(TestCase):
    def setUp(self):
        # create new user
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True

    def test_valid_profile_form(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

                post_data = {
                    'origin': 'US',
                    'residence': 'US',
                    'phone_number': '+989119941033',
                    'gender': 'm',
                    'bio': 'lorem ipsum',
                    'location': 'Tehran, Iran',
                    'social_media': 'instagram.com/sepehr.ak',
                }

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()

                file_data = {
                    'picture': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
                    'id_card': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                }

                form = ProfileDraftForm(post_data, file_data, instance=self.user)

                self.assertTrue(form.is_valid(), msg='Profile Draft `Valid` Form is not valid. \n\n \
                                                      +++++++++++++++++++++\n value: {}'.format(form.errors))


class ProfileInValidDraftFormTest(TestCase):
    def setUp(self):
        # create new user
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')

    def test_invalid_min_length_phone_number(self):
        post_data = {
            'phone_number': '1234',
        }
        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='min length phone number SHOULD\'NT be valid.')

    def test_invalid_max_length_phone_number(self):
        post_data = {
            'phone_number': '12345678912345',
        }
        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='max length phone number SHOULD\'NT be valid.')

    def test_invalid_format_phone_number(self):
        post_data = {
            'phone_number': '91199413311',
        }
        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='format phone number SHOULD\'NT be valid.')

    def test_invalid_gender(self):
        post_data = {
            'gender': 'z',
        }
        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='gender SHOULD\'NT be valid.')

    def test_invalid_is_published(self):
        post_data = {
            'is_published': True,
        }
        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='is published in draft form must be false.')

    def test_invalid_picture(self):
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/250x200.jpg'), 'rb') as uploaded_picture:
            post_data = dict()

            uploaded_picture_name = uploaded_picture.name
            uploaded_picture_file = uploaded_picture.read()

            file_data = {
                'picture': SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file),
            }

            form = ProfileDraftForm(post_data, file_data, instance=self.user)
            self.assertFalse(form.is_valid(), msg='`picture` must be invalid but logic has accepted.')

    def test_invalid_social_media(self):
        post_data = {
            'social_media': 'abcd'
        }
        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`social_media` must be invalid but logic has accepted.')

    def test_invalid_origin_country(self):
        post_data = {
            'origin': 'PP'
        }

        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`origin` must be invalid but logic has accepted.')

    def test_invalid_residence_country(self):
        post_data = {
            'residence': 'OO'
        }

        form = ProfileDraftForm(post_data, instance=self.user)
        self.assertFalse(form.is_valid(), msg='`residence` must be invalid but logic has accepted.')


class ProfileValidPublishedFormTest(TestCase):
    def setUp(self):
        # create new user
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True

        # make a valid form to publish
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()

                self.user.profile.origin = 'US'
                self.user.profile.residence = 'US'
                self.user.profile.phone_number = '+0989119941033'
                self.user.profile.gender = 'm'
                self.user.profile.bio = 'lorem ipsum'
                self.user.profile.location = 'Tehran, Iran'
                self.user.profile.social_media = 'instagram.com/sepehr.ak'
                self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)

    def test_valid_profile_form(self):
        post_data = {
            'is_published': True
        }
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertTrue(form.is_valid(), msg='Profile Published `Valid` Form is not valid. \n\n \
                                                      +++++++++++++++++++++\n value: {}'.format(form.errors))


class ProfileInValidPublishedFormTest(TestCase):
    def setUp(self):
        # create new user
        self.user = User.objects.create(username='test_user', email='sepehr.ak@gmail.com', password='$epehr@1234')
        self.user.profile.email_confirmed = True

        # make a valid form to publish
        with open(os.path.join(settings.BASE_DIR, 'painless/pictures/300.jpg'), 'rb') as uploaded_picture:

                uploaded_picture_name = uploaded_picture.name
                uploaded_picture_file = uploaded_picture.read()

                self.user.profile.origin = 'US'
                self.user.profile.residence = 'US'
                self.user.profile.phone_number = '+0989119941033'
                self.user.profile.gender = 'm'
                self.user.profile.bio = 'lorem ipsum'
                self.user.profile.location = 'Tehran, Iran'
                self.user.profile.social_media = 'instagram.com/sepehr.ak'
                self.user.profile.id_card = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)
                self.user.profile.picture = SimpleUploadedFile(uploaded_picture_name, uploaded_picture_file)

    def test_invalid_origin_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.origin = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`origin` must be invalid but logic has accepted.')

    def test_invalid_residence_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.residence = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`residence` must be invalid but logic has accepted.')

    def test_invalid_phone_number_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.phone_number = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`phone_number` must be invalid but logic has accepted.')
    
    def test_invalid_gender_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.gender = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`gender` must be invalid but logic has accepted.')
    
    def test_invalid_bio_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.bio = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`bio` must be invalid but logic has accepted.')
    
    def test_invalid_location_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.location = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`location` must be invalid but logic has accepted.')
    
    def test_invalid_social_media_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.social_media = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`social_media` must be invalid but logic has accepted.')

    def test_invalid_picture_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.picture = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`picture` must be invalid but logic has accepted.')
    
    def test_invalid_id_card_null(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.id_card = None
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`id_card` must be invalid but logic has accepted.')

    def test_invalid_email_confirmed_false(self):
        post_data = {
            'is_published': True
        }
        self.user.profile.email_confirmed = False
        form = ProfilePublishForm(post_data, instance=self.user)

        self.assertFalse(form.is_valid(), msg='`email_confirmed` must be invalid but logic has accepted.')


class LoginTest(TestCase):

    def setUp(self):

        self.user = User.objects.create(
            username='hamed_test',
            email='hmdbbgh1011@gmail.com',
            password='h@med_1234'
        )

        # self.client = Client(enforce_csrf_checks = True)
        self.client = Client()

    
    def test_login(self):

        response = self.client.post('/accounts/login/', {'username': 'hamed_test', 'password': 'h@med_1234'})
        
        self.assertEqual(
            response.status_code,
            200
        )
