from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render
from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic import View
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.views import auth_login
from django.contrib.auth.forms import AuthenticationForm

from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

from django.utils.encoding import force_text
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode
from django.utils.http import urlsafe_base64_encode

from django.contrib.auth.tokens import PasswordResetTokenGenerator
# from django.utils import six
from django.core.mail import send_mail

from .forms import SignUpForm

from accounts.subviews.profile import (
    AccountProfileUpdateView,
    AccountProfileFinalizeView
)


# class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
#     def _make_hash_value(self, user, timestamp):
#         return (
#             six.text_type(user.pk) + six.text_type(timestamp) +
#             six.text_type(user.profile.email_confirmed)
#         )

# account_activation_token = AccountActivationTokenGenerator()

# class RegisterView(SuccessMessageMixin, View):
#     view_data = {"page_name": 'registration'}

#     def get(self, request, *args, **kwargs):
#         form = SignUpForm()
#         return render(request, 'registration/signup.html', {"form": form, "view": self.view_data})

#     def post(self, request, *args, **kwargs):
#         form = SignUpForm(request.POST)

#         if form.is_valid():
#             user = form.save(commit=False)

#             if self.recaptcha_validation(form):
#                 user.is_active = False
#                 user.save()
#                 self.send_activation_email(request, user, "deneb")       
#                 messages.success(request, 'You registered Successfully. Please visit your email address and activate your account.')
#                 return redirect('accounts:register')
#             else:
#                 messages.error(request, 'Captcha Failure.')
#                 return redirect('accounts:register')

    
#         messages.error(request, 'You have some errors.')
#         return render(request, 'registration/signup.html', {"form": form, "view": self.view_data})


#     def recaptcha_validation(self, form):
#         from django.conf import settings
#         import requests
        
#         secret_key = settings.RECAPTCHA_SECRET_KEY
        
#         data = {
#             'response': form.data['g-recaptcha-response'],
#             'secret': secret_key
#         }

#         resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
#         result_json = resp.json()

#         return True if result_json.get('success') else False

#     def send_activation_email(self, request, user, website_name, *args, **kwargs):
#         # send an email to registered user.
#         current_site = get_current_site(request)
#         subject = 'Activate Your {} Account'.format(website_name)
        
#         message = render_to_string('mail/account_activation_email.html', {
#             'request': request,
#             'user': user,
#             'domain': current_site.domain,
#             'uid': urlsafe_base64_encode(force_bytes(user.pk)),
#             'token': account_activation_token.make_token(user),
#         })

#         send_mail(subject, 'html/txt', 'denebtestify@gmail.com', [user.email,], html_message=message)
#         # user.email_user(subject, message)

# def activate(request, uidb64, token):
#     try:
#         uid = force_text(urlsafe_base64_decode(uidb64))
#         user = User.objects.get(pk=uid)
#     except (TypeError, ValueError, OverflowError, User.DoesNotExist):
#         user = None

#     if user is not None and account_activation_token.check_token(user, token):
#         user.is_active = True
#         user.profile.email_confirmed = True
#         user.save()
#         login(request, user)
#         return render(request, 'registration/account_activation_valid.html')
#     else:
#         return render(request, 'registration/account_activation_invalid.html')

